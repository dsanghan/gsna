package datatypes.simulator;

import java.util.HashSet;

public class Node extends Point
{
    public HashSet<Node> connections;
    public int pathLength = 0;
    public Node previous;
    public boolean visited = false;
    private int PRIME_X = 17;
    private int PRIME_Y = 19;

    public Node()
    {
        super(0, 0);
        connections = new HashSet<Node>();
    }

    public Node(double x, double y)
    {
        super(x, y);
        connections = new HashSet<Node>();
    }

    public int hashCode()
    {
        return (int)(PRIME_X * 10000 * x + PRIME_Y * 10000 * y);
    }

    public boolean equals(Object object)
    {
        if (!(object instanceof Node))
            return false;

        Node node = (Node) object;

        return node.getX() == x && node.getY() == y;
    }


}
