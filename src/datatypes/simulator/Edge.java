package datatypes.simulator;

/**
 * User: Dev
 * Date: 4/24/13
 * Time: 12:20 AM
 */
public class Edge
{
    public Point p1;
    public Point p2;
    public Entity parent;

    public Edge(Point p1, Point p2, Entity parent)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.parent = parent;
    }

    public double distance()
    {
        return p1.subtract(p2).length();
    }

    public String toString()
    {
        return "Edge goes from: " + p1.toString() + " to: " + p2.toString();
    }
}
