package datatypes.simulator;

import utils.Constants;
import utils.GeometryTools;
import utils.Logger;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 4/25/13
 * Time: 12:49 AM
 */
public class Robot extends Entity
{
    public double x;
    public double y;
    public double orientation;
    public double visData[];
    public datatypes.simulator.Point position;
    public datatypes.simulator.Point goal;
    public datatypes.simulator.Point boundaryStart;
    private double speed = 0.01;
    private boolean lost = false;
    private boolean found = false;
    private double turnrate;
    private RobotMode robotMode = RobotMode.MOVE_TO_TARGET;
    public ArrayList<Edge> tangents;
    private boolean move = false;
    private boolean reached = false;
    private boolean firstIter = true;
    private datatypes.simulator.Point targetPosition;
    private double targetOrientation;
    private double dMin = 0;
    private static Logger logger = Logger.getLogger("Robot");

    public Robot()
    {
        super(EntityType.ROBOT);
        init();
    }

    public Robot(double x, double y)
    {
        super(EntityType.ROBOT);
        this.x = x;
        this.y = y;
        position = new datatypes.simulator.Point(x, y);
        init();
    }

    public void init()
    {
        this.orientation = 0;
        visData = new double[Constants.robotResolution];
        tangents = new ArrayList<Edge>();
        targetPosition = new datatypes.simulator.Point(x, y);
    }

    public double[] getX()
    {
        return new double[]{this.x};
    }

    public double[] getY()
    {
        return new double[]{this.y};
    }

    public double getOrientation() {
        return orientation;
    }

    public void readLaserData(Workspace w)
    {
        for (int i = 0; i < visData.length; i++)
            visData[i] = Constants.visibility;

        int index = 0;
        for (double i = 0; i < Constants.viewAngle; i += Constants.thetaIncrement, index++)
        {
            datatypes.simulator.Point pc = position.createPointOnCircle(i + orientation, Constants.visibility);
            Edge e = new Edge(position, pc, this);

            for (Obstacle o : w.getObstacleMap())
            {
                if (o.inRange(position))
                {
                    for (Edge curr : o.getEdges())
                    {
                        if (GeometryTools.intersects(e, curr))
                        {
                            datatypes.simulator.Point intersection = GeometryTools.getIntersectionPoint(e.p1, e.p2, curr.p1, curr.p2);
                            double distanceToGoal = intersection.distanceTo(goal);
                            dMin = Math.min(distanceToGoal, dMin);
                            double distance = position.distanceTo(intersection);
                            if (visData[index] == 0)
                                visData[index] = distance;
                            else
                                visData[index] = Math.min(distance,visData[index]);
                        }
                    }
                }
            }

            if (false)
            for (Edge curr : w.getEdges())
            {
                if (GeometryTools.intersects(e, curr))
                {
                    datatypes.simulator.Point intersection = GeometryTools.getIntersectionPoint(e.p1, e.p2, curr.p1, curr.p2);
                    double distance = position.distanceTo(intersection);
                    if (visData[index] == 0)
                        visData[index] = distance;
                    else
                        visData[index] = Math.min(distance,visData[index]);
                    break;
                }
            }
        }
    }

    public void generatePaths(Workspace w)
    {
        ArrayList<Edge> paths = new ArrayList<Edge>();

        boolean intersects = false;
        double angle = GeometryTools.calculateAngleBetweenVectors(goal, position, new datatypes.simulator.Point(position.getX() + 100, position.getY()));

        if (goal.getY() - position.getY() < 0)
            angle = -angle;

        Edge goalEdge;

        if (position.distanceTo(goal) < Constants.visibility)
            goalEdge = new Edge(position, goal, this);

        else
            goalEdge = new Edge(position, position.createPointOnCircle(angle, Constants.visibility), this);

        Edge rayCheck = new Edge(goalEdge.p2, new datatypes.simulator.Point(goalEdge.p2.getX() + 100, goalEdge.p2.getY()), this);

        double prev = visData[visData.length - 1];

        for (int i = 0; i < visData.length; i++)
        {
            double curr = visData[i];
            if (prev == Constants.visibility && curr != Constants.visibility)
            {
                paths.add(new Edge(position, position.createPointOnCircle(orientation + i/2, curr - 0.1), this));
            }
            else if (prev != Constants.visibility && curr == Constants.visibility)
            {
                paths.add(new Edge(position, position.createPointOnCircle(orientation + i/2, prev - 0.1), this));
            }

            prev = curr;
        }

        for (Obstacle o : w.getObstacleMap())
        {
            int edgeIntersects = 0;

            /*
            if (o.inRange(position))
            {
                ArrayList<Edge> obsTans = GeometryTools.findTangentsToObstacle(position, o);
                for (Edge e : obsTans)
                    if (e.distance() < Constants.visibility && !GeometryTools.intersectsObstacle(o, e))
                        paths.add(e);
                //paths.addAll(GeometryTools.findTangentsToObstacle(position, o));
            }
            */

            for (Edge e : o.getEdges())
            {
                if (GeometryTools.intersects(rayCheck, e))
                    edgeIntersects++;

                if (GeometryTools.intersects(goalEdge, e))
                    intersects = true;
                /*
                if (!firstIter)
                {
                    if (e.p1.equals(targetPosition))
                        paths.add(new Edge(position, e.p2, o));
                    else if (e.p2.equals(targetPosition))
                        paths.add(new Edge(position, e.p1, o));
                    else if (GeometryTools.intersects(e, goalEdge))
                        intersects = true;
                }
                */
            }

            if (edgeIntersects % 2 != 0)
                intersects = true;
        }

        if (!intersects)
        {
            paths.add(goalEdge);
            //logger.log(Logger.LogLevel.INFO, "Adding goal path: " + goalEdge.toString());
        }

        tangents = paths;
    }

    public datatypes.simulator.Point findMTGTarget(Workspace w)
    {
        generatePaths(w);
        double minDistance = dMin;
        datatypes.simulator.Point bestTarget = null;

        for (Edge e : tangents)
        {
            double distance = e.p2.distanceTo(goal);
            //logger.log(Logger.LogLevel.INFO, e.toString() + " with distance to goal: " + distance);
            if (distance <= minDistance)
            {
                minDistance = distance;
                bestTarget = e.p2;
            }
        }

        if (bestTarget == null || bestTarget.equals(targetPosition))
        {
            robotMode = RobotMode.BOUNDARY_FOLLOW;
            return null;
        }

        if (GeometryTools.insideAnyObstacle(bestTarget, w.getObstacleMap()))
        {
            dMin = Math.min(dMin, bestTarget.distanceTo(goal));
            robotMode = RobotMode.BOUNDARY_FOLLOW;
            return null;
        }

        firstIter = false;
        targetOrientation = GeometryTools.calculateAngleBetweenVectors(bestTarget, position, new datatypes.simulator.Point(position.getX() + 100, position.getY()));

        if (bestTarget.getY() - position.getY() < 0)
            targetOrientation = -targetOrientation;

        //logger.log(Logger.LogLevel.INFO, "Moving to: " + targetPosition + " at angle: " + targetOrientation);

        move = true;

        return bestTarget;
    }

    private void findBFTarget()
    {
        double desiredAngle = getDirection();
        //boundaryTarget = new Point(minPoint.x + Constants.minDistanceX * )
        if (minPoint != null)
        {
            double distance = minPoint.distanceTo(goal);
            if (distance < dMin)
                dMin = distance;
        }

        double turnrate = getTurnRate(desiredAngle);

        speed = 0.1;

        double newOrientation = orientation + turnrate;
        //speed = Math.max(0, (0.5 - Math.abs(turnrate))/50);

        double angle = Math.toRadians(newOrientation);

        double dx = speed * Math.cos(angle);
        double dy = speed * Math.sin(angle);

        datatypes.simulator.Point newPosition = new datatypes.simulator.Point(position.getX() + dx, position.getY() + dy);
        datatypes.simulator.Point newMinPoint = findMinPointToPoint(newPosition);

        double newAngle = GeometryTools.calculateAngleBetweenVectors(newPosition, newMinPoint, new datatypes.simulator.Point(newMinPoint.getX() + 100, newMinPoint.getY()));

        if (newPosition.getY() < newMinPoint.getY())
            newAngle = -newAngle;

        newAngle = Math.toRadians(newAngle);

        targetPosition = new datatypes.simulator.Point(newMinPoint.getX() + Constants.minDistanceX * Math.cos(newAngle),
                                   newMinPoint.getY() + Constants.minDistanceY * Math.sin(newAngle));
        targetOrientation = GeometryTools.calculateAngleBetweenVectors(targetPosition, position, new datatypes.simulator.Point(position.getX() + 100, position.getY()));
        if (targetPosition.getY() < position.getY())
            targetOrientation = -targetOrientation;
    }

    private datatypes.simulator.Point findMinPointToPoint(datatypes.simulator.Point point)
    {
        double currMinDistance = Constants.visibility;

        datatypes.simulator.Point minPoint = null;

        for (int i = 0; i < visData.length; i++)
        {
            int currIndex = (i + 540) % 720;

            if (visData[currIndex] > 0)
            {
                double angle = Math.toRadians((currIndex/2) + orientation);
                datatypes.simulator.Point currPoint = new datatypes.simulator.Point(position.x + visData[currIndex]*Math.cos(angle), position.y + visData[currIndex]*Math.sin(angle));

                double distance = currPoint.distanceTo(point);

                if (distance < currMinDistance)
                {
                    currMinDistance = distance;
                    minPoint = currPoint;
                }
            }
        }

        return minPoint;
    }

    private double getTurnRate(double desiredOrientation)
    {
        if (desiredOrientation < 0)
            desiredOrientation += 360;

        if (orientation < 0)
            orientation += 360;

        if (Math.abs(desiredOrientation - orientation) > 180)
        {
            if (orientation > 180)
                desiredOrientation += 360;
            else
                orientation += 360;
        }

        double turnrate = desiredOrientation - orientation;
        if (orientation >= 360)
            orientation -= 360;

        if (Math.abs(turnrate) > Constants.maxTurnRate)
            turnrate = turnrate * Constants.maxTurnRate / Math.abs(turnrate);

        return turnrate;
    }

    private boolean boundaryTerminate = false;
    private boolean boundaryFirstIter = false;

    public void update(Workspace w)
    {
        //System.out.println("Position: " + position + " Target: " + (targetPosition == null ? "" : targetPosition.toString()));

        if (reached || Constants.pause)
            return;

        readLaserData(w);
        //generatePaths(w);

        if (robotMode == RobotMode.BOUNDARY_FOLLOW)
        {
            datatypes.simulator.Point MTG_POSITION = findMTGTarget(w);
            if (MTG_POSITION != null && !MTG_POSITION.equals(targetPosition))
            {
                robotMode = RobotMode.MOVE_TO_TARGET;
                move = false;
                return;
            }

            if (!move)
            {
                findBFTarget();
                move = true;
                if (boundaryFirstIter)
                {
                    boundaryStart = targetPosition;
                    boundaryTerminate = false;
                }
            }

            else
            {
                turnrate = getTurnRate(targetOrientation);

                if (Math.abs(turnrate) > Constants.maxTurnRate)
                    turnrate = turnrate * Constants.maxTurnRate / Math.abs(turnrate);

                orientation += turnrate;

                if (turnrate != 0)
                    speed = 0;
                else
                {
                    speed = position.distanceTo(targetPosition);
                    speed = speed > 0.01 ? 0.01 : speed;
                }

                double angle = Math.toRadians(orientation);

                double dx = speed * Math.cos(angle);
                double dy = speed * Math.sin(angle);

                position.addX(dx);
                position.addY(dy);
                x += dx;
                y += dy;

                if (position.distanceTo(targetPosition) < Constants.thresholdDistance)
                {
                    move = false;
                    if (boundaryFirstIter)
                    {
                        boundaryFirstIter = false;
                        boundaryTerminate = false;
                    }
                }

                if (position.distanceTo(goal) < Constants.thresholdDistance)
                    reached = true;

                if (boundaryTerminate && position.distanceTo(boundaryStart) < Constants.thresholdDistance)
                {
                    System.out.println("NO PATH FOUND.... Terminating...");
                    Constants.pause = true;
                }
                else if (!boundaryFirstIter && position.distanceTo(boundaryStart) > Constants.thresholdDistance)
                {
                    boundaryTerminate = true;
                }
            }
        }

        else if (robotMode == RobotMode.MOVE_TO_TARGET)
        {
            if (!move)
            {
                targetPosition = findMTGTarget(w);
                if (targetPosition == null)
                {
                    robotMode = RobotMode.BOUNDARY_FOLLOW;
                    boundaryTerminate = false;
                    boundaryFirstIter = true;
                    return;
                }
            }

            else
            {
                turnrate = getTurnRate(targetOrientation);

                if (Math.abs(turnrate) > Constants.maxTurnRate)
                    turnrate = turnrate * Constants.maxTurnRate / Math.abs(turnrate);

                orientation += turnrate;

                if (turnrate != 0)
                    speed = 0;
                else
                    speed = 0.01;

                double angle = Math.toRadians(orientation);

                double dx = speed * Math.cos(angle);
                double dy = speed * Math.sin(angle);

                position.addX(dx);
                position.addY(dy);
                x += dx;
                y += dy;

                if (position.distanceTo(targetPosition) < Constants.thresholdDistance)
                {
                    move = false;
                }

                if (position.distanceTo(goal) < Constants.thresholdDistance)
                    reached = true;
            }
        }

        double distance = position.distanceTo(goal);
        if (distance < dMin)
            dMin = distance;
    }

    private double minDistance = Constants.visibility;
    public datatypes.simulator.Point minPoint;

    public double getDirection()
    {
        int minIndex = -1;
        int maxIndex = -1;
        minPoint = null;
        double currMinDistance = Constants.visibility;

        for (int i = 0; i < visData.length; i++)
        {
            int currIndex = (i + 540) % 720;

            if (visData[currIndex] > 0)
            {
                if (minIndex == -1)
                    minIndex = currIndex;
                else
                    maxIndex = currIndex;

                if (visData[currIndex] < currMinDistance)
                {
                    currMinDistance = visData[currIndex];
                    double angle = Math.toRadians((currIndex/2) + orientation);
                    minPoint = new datatypes.simulator.Point(position.x + currMinDistance*Math.cos(angle), position.y + currMinDistance*Math.sin(angle));
                }
                currMinDistance = Math.min(visData[currIndex], currMinDistance);
            }
        }

        minDistance = currMinDistance;

        if (minIndex == -1 || maxIndex == -1 || minIndex == maxIndex)
        {
            lost = true;
            return orientation;
        }

        found = true;
        lost = false;

        double angle1 = Math.toRadians(orientation + (minIndex / 2));
        double angle2 = Math.toRadians(orientation + (maxIndex / 2));

        double x1 = x + visData[minIndex] * Math.cos(angle1);
        double y1 = y + visData[minIndex] * Math.sin(angle1);

        double x2 = x + visData[maxIndex] * Math.cos(angle2);
        double y2 = y + visData[maxIndex] * Math.sin(angle2);

        double x3 = x2 + 100;
        double y3 = y2;

        double angle = GeometryTools.calculateAngleBetweenVectors(new datatypes.simulator.Point(x1, y1), new datatypes.simulator.Point(x2, y2), new datatypes.simulator.Point(x3, y3));

        if (y1 - y2 < 0)
            angle = -angle;

        //System.out.println(minIndex + " : " + maxIndex + "("+x1+", "+y1+")" + "("+x2+", "+y2+") : " + angle + " : " + orientation);

        return angle;
    }

    public datatypes.simulator.Point getTargetPosition()
    {
        return targetPosition;
    }

    public void setGoal(double x, double y)
    {
        goal = new datatypes.simulator.Point(x, y);
        dMin = position.distanceTo(goal);
    }
}
