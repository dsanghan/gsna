package datatypes.simulator;

import datatypes.object.Vertex;
import org.ejml.simple.SimpleMatrix;

public class Point extends SimpleMatrix
{
    double x = 0;
    double y = 0;

    public Point(double x, double y)
    {
        super(new double[][] {{x, y}});
        this.x = x;
        this.y = y;
    }

    public Point(Point p)
    {
        new Point(p.x, p.y);
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public Point subtract(Point point)
    {
        return new Point(this.x - point.getX(), this.y - point.getY());
    }

    public double dot(Point point)
    {
        return this.x * point.getX() + this.y * point.getY();
    }

    public double length()
    {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public String toString()
    {
        return "X: " + x + " Y: " + y;
    }

    public Point createPointOnCircle(double theta, double distance)
    {
        double angle = Math.toRadians(theta);
        return new Point(this.x + (distance * Math.cos(angle)), this.y + (distance * Math.sin(angle)));
    }

    public double distanceTo(Point p)
    {
        return Math.sqrt((p.getX() - x)*(p.getX() - x) + (p.getY() - y)*(p.getY() - y));
    }

    public void addX(double x)
    {
        this.x += x;
    }

    public void addY(double y)
    {
        this.y += y;
    }

    public boolean equals(Object obj)
    {
        if (!(obj instanceof Point))
            return false;

        Point curr = (Point) obj;

        return curr.getX() == x && curr.getY() == y;
    }

    public Vertex getVertex()
    {
        return new Vertex(this.x, this.y, 0);
    }
}
