package datatypes.simulator;

import datatypes.object.Face;
import datatypes.object.Obj;
import utils.Constants;
import utils.GLTools;

import java.util.ArrayList;
import java.util.HashSet;

public class Workspace extends Entity
{
    HashSet<Obstacle> obstacles;
    HashSet<Robot> robots;
    private Robot robot;
    public Obj workspaceObj;

    public Workspace()
    {
        super(EntityType.WORKSPACE);
        init();
    }

    public Workspace(ArrayList<Point> points)
    {
        super(points, EntityType.WORKSPACE);
        init();
    }

    public void init()
    {
        obstacles = new HashSet<Obstacle>();
        robots = new HashSet<Robot>();
        generateObj();
    }

    public void generateObj()
    {
        workspaceObj = Obj.getObj("workspace");
        ArrayList<Point> points = this.getPoints();

        Point p1 = points.get(0);
        Point p2 = points.get(1);
        Point p3 = points.get(2);
        Point p4 = points.get(3);

        Face f = GLTools.generateFace(p1.getVertex(),
                p2.getVertex(),
                p3.getVertex(),
                p4.getVertex(), Constants.workspaceMaterial);
        workspaceObj.addFace(f);
    }

    public HashSet<Obstacle> getObstacleMap()
    {
        return obstacles;
    }

    public void addObstacle(Obstacle obstacle)
    {
        obstacles.add(obstacle);
    }

    public void setRobot(Robot r)
    {
        this.robot = r;
    }

    public Robot getRobot() {
        return robot;
    }

    public void tick()
    {
        robot.update(this);
    }
}