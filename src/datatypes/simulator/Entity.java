package datatypes.simulator;

import com.google.common.primitives.Doubles;
import utils.Constants;
import utils.GeometryTools;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 4/24/13
 * Time: 12:18 AM
 */
public class Entity
{
    private ArrayList<Point> points;
    private ArrayList<Point> tangentPoints;
    private ArrayList<Edge> edges;
    protected Point center;
    private ArrayList<Double> X;
    private ArrayList<Double> Y;
    private EntityType type;
    public double maxDistance;

    public Entity(EntityType type)
    {
        points = new ArrayList<Point>();
        init();
        this.type = type;
    }

    public Entity(ArrayList<Point> points, EntityType type)
    {
        if (points.size() < 3 && type != EntityType.ROBOT)
            throw new Error("An polygon must have atleast 3 points.");
        this.points = points;
        this.type = type;
        init();
        generateEdges();
    }

    private void init()
    {
        edges = new ArrayList<Edge>();
        X = new ArrayList<Double>();
        Y = new ArrayList<Double>();
        tangentPoints = new ArrayList<Point>();
    }

    public ArrayList<Point> getPoints()
    {
        return points;
    }

    public void addPoint(Point p)
    {
        points.add(p);

        if (points.size() == 3)
            generateEdges();

        else if (points.size() > 3)
        {
            edges.remove(edges.size() - 1);
            edges.add(new Edge(points.get(points.size() - 2), p, this));
            edges.add(new Edge(p, points.get(0), this));
            X.add(p.getX());
            Y.add(p.getY());
        }

        generateCenter();
    }

    private void generateCenter()
    {
        double cx = 0, cy = 0;

        for (Point p : points)
        {
            cx += p.getX();
            cy += p.getY();
        }

        center = new Point(cx / points.size(), cy / points.size());
        generateMaxDistance();
    }

    private void generateMaxDistance()
    {
        maxDistance = 0;

        for (Point p : points)
        {
            double distance = p.distanceTo(center);
            if (distance > maxDistance)
                maxDistance = distance;
        }
    }

    private void generateEdges()
    {
        Point first = points.get(0);

        for (int i = 0; i < points.size() - 1; i++)
        {
            Point currpoint = points.get(i);
            edges.add(new Edge(currpoint, points.get(i + 1), this));
            X.add(currpoint.getX());
            Y.add(currpoint.getY());
        }

        Point lastPoint = points.get(points.size() - 1);
        edges.add(new Edge(lastPoint, first, this));
        X.add(lastPoint.getX());
        X.add(first.getX());
        Y.add(lastPoint.getY());
        Y.add(first.getY());

        generateCenter();
        generateTangentPoints();
    }

    protected static void generateEdgesFromPoints(ArrayList<Edge> edges, ArrayList<Point> points, Entity entity)
    {
        Point first = points.get(0);

        for (int i = 0; i < points.size() - 1; i++)
        {
            Point currpoint = points.get(i);
            edges.add(new Edge(currpoint, points.get(i + 1), entity));
        }

        Point lastPoint = points.get(points.size() - 1);
        edges.add(new Edge(lastPoint, first, entity));
    }

    public void generateTangentPoints()
    {
        tangentPoints = GeometryTools.jarvisMarch((ArrayList<Point>)points.clone());
    }

    public ArrayList<Edge> getEdges()
    {
        return edges;
    }

    public double[] getX()
    {
        return Doubles.toArray(X);
    }

    public double[] getY()
    {
        return Doubles.toArray(Y);
    }

    public void rotate(double theta)
    {

    }

    public Point getCenter()
    {
        return center;
    }

    public boolean inRange(Point p)
    {
        double currDistance = p.distanceTo(getCenter());

        if (currDistance <= maxDistance + Constants.visibility)
            return true;

        return false;
    }

    public ArrayList<Point> getTangentPoints() {
        return tangentPoints;
    }
}
