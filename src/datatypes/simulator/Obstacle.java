package datatypes.simulator;

import datatypes.object.Face;
import datatypes.object.Obj;
import datatypes.object.Vertex;
import utils.Constants;
import utils.GLTools;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 4/24/13
 * Time: 12:33 PM
 */
public class Obstacle extends Entity
{
    private static int obstacleIndex = 0;
    private String name;

    public Obj obsObject;
    public ArrayList<Point> renderPoints;

    public Obstacle()
    {
        super(EntityType.OBSTACLE);
        this.name = "obstacle_" + obstacleIndex++;
        renderPoints = new ArrayList<Point>();
    }

    public Obstacle(ArrayList<Point> points)
    {
        super(points, EntityType.OBSTACLE);
        generateObj();
        renderPoints = new ArrayList<Point>();
    }

    public Obstacle(ArrayList<Point> points, ArrayList<Point> renderPoints)
    {
        super(points, EntityType.OBSTACLE);
        this.renderPoints = renderPoints;
        generateObj();
    }

    public void generateObj()
    {
        obsObject = Obj.getObj(this.name);

        ArrayList<Edge> edges = new ArrayList<Edge>();
        generateEdgesFromPoints(edges, renderPoints, this);

        for (Edge e : edges)
        {
            Point p1 = e.p1;
            Point p2 = e.p2;

            Vertex v1 = new Vertex(p1.x, p1.y, Constants.BASE_HEIGHT);
            Vertex v2 = new Vertex(p2.x, p2.y, Constants.BASE_HEIGHT);
            Vertex v3 = new Vertex(p2.x, p2.y, Constants.OBJ_HEIGHT);
            Vertex v4 = new Vertex(p1.x, p1.y, Constants.OBJ_HEIGHT);

            Face f = GLTools.generateFace(v1, v2, v3, v4, Constants.obstacleMaterial);

            //System.out.println(p1.toString() + ", " + p2.toString() + " -> " + x1 + " : " + y1 + ", " + x2 + " : " + y2);

            obsObject.addFace(f);
        }
    }
}
