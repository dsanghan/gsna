package datatypes.simulator;

/**
 * User: Dev
 * Date: 4/25/13
 * Time: 12:49 AM
 */
public enum EntityType
{
    ROBOT, OBSTACLE, WORKSPACE;
}
