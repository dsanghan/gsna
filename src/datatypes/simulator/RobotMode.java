package datatypes.simulator;

/**
 * User: Dev
 * Date: 5/18/13
 * Time: 7:01 PM
 */
public enum RobotMode
{
    BOUNDARY_FOLLOW, MOVE_TO_TARGET;
}
