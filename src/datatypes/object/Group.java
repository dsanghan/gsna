package datatypes.object;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 5/30/13
 * Time: 1:34 PM
 */
public class Group
{
    public ArrayList<Face> faces;
    public String material;
    public int facePoints;
    public int numVerts = 0;

    public Group(String name)
    {
        faces = new ArrayList<Face>();
        numVerts = 0;
    }

    public void addFace(Face face)
    {
        faces.add(face);
        material = face.material;
        facePoints = face.V.size();
    }

    public ArrayList<Face> getFaces()
    {
        return faces;
    }

    public void genNumEverything()
    {
        numVerts = 0;

        for (Face f : faces)
        {
            for (Vertex v : f.getV())
                numVerts++;
        }
    }
}
