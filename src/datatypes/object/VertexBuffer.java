package datatypes.object;

import utils.Constants;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * User: Dev
 * Date: 5/30/13
 * Time: 1:47 PM
 */
public class VertexBuffer
{
    public int VBO[] = null;
    private GL2 gl;
    private ArrayList<Group> groups;
    private FloatBuffer vertices, normals, textures, colors;

    public VertexBuffer(GL2 gl, ArrayList<Group> groups)
    {
        this.gl = gl;
        this.groups = groups;

        VBO = new int[groups.size() * 4];
        gl.glGenBuffers(groups.size() * 4, VBO, 0);
        generateBuffers();
    }

    private void generateBuffers()
    {
        // Loop through all of the objects we have
        for (int i=0; i< groups.size(); i++) {
            // Get the current object from which we'll retrieve the vertices and normals
            Group group = groups.get(i);
            group.genNumEverything();

            // First create the VERTICES buffer for each object
            //  ...corresponding to the VBO[4*i] ids
            // Bind the buffer to represent which buffer we are currently working with
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, VBO[4*i]);

            // Allocate the float buffer for the vertices.  The size of the
            // buffer of vertices corresponds to how many faces/triangles
            // each object has times 3 vertices per triangle times 3 floats
            // per vertex:  #faces * 3 * 3
            int totalBufferSize = group.numVerts*3;
            vertices = FloatBuffer.allocate(totalBufferSize);

            // Create the float buffer for the vertices by looping over each
            // face and adding the coordinates of each vertex to the buffer
            // in the order that the vertices would normally be called in
            // a direct draw method.  Yes, you will be duplicating the vertices
            // across the buffer because vertices are typically shared by
            // faces/triangles, but that's how it is supposed to be done.
            for (int j=0; j<group.faces.size(); j++) {
                // Loop through the 3 vertices of each face
                for (int whichVertex=0; whichVertex<group.faces.get(j).V.size(); whichVertex++) {
                    Vertex v = group.faces.get(j).V.get(whichVertex);

                    // add the X, Y, Z float values of the current vertex to
                    // the 'vertices' float buffer
                    vertices.put((float) (v.x / Constants.scale));
                    vertices.put((float) (v.y / Constants.scale));
                    vertices.put((float) (v.z / Constants.scale));
                }
            }

            // Rewind buffer ... necessary step
            vertices.rewind();

            // Write out vertex buffer to the currently bound VBO.
            // Use 'totalBufferSize*4' because we have 4 bytes for a float.
            gl.glBufferData(GL.GL_ARRAY_BUFFER, totalBufferSize *4, vertices, GL.GL_STATIC_DRAW);

            // Free the vertices buffer since we are done with them
            vertices = null;


            // Second create the NORMALS buffer for each object
            //  ...corresponding to the VBO[4*i+1] ids
            // Bind the buffer to represent which buffer we are currently working with
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, VBO[4*i+1]);

            // Allocate the float buffer for the normals.  The size of the
            // buffer of normals corresponds to the same number of floats
            // as the vertices since each vertex has a normal and each normal
            // also has 3 floats so the same buffer size is used.
            totalBufferSize = group.numVerts*3;
            normals = FloatBuffer.allocate(totalBufferSize);

            // Create the float buffer for the normals by looping over each
            // face and each face's vertex and adding the coordinates of
            // each corresponding normal to the buffer...same concept as for
            // vertices
            for (int j = 0; j < group.faces.size(); j++) {
                // Loop through the 3 normals of each face
                for (int whichVertex=0; whichVertex<group.faces.get(j).VN.size(); whichVertex++) {
                    // add the X, Y, Z float values of the current normal to
                    // the 'vertices' float buffer
                    if (group.faces.get(j).VN.size() != 0)
                    {
                        Vertex v = group.faces.get(j).VN.get(whichVertex);
                        normals.put((float) v.x);
                        normals.put((float) v.y);
                        normals.put((float) v.z);
                    }
                    else
                    {
                        normals.put(0);
                        normals.put(0);
                        normals.put(0);
                    }
                }
            }

            // Rewind buffer
            normals.rewind();

            // Write out the normals buffer to the currently bound VBO.
            // Use 'totalBufferSize*4' because we have 4 bytes for a float.
            gl.glBufferData(GL.GL_ARRAY_BUFFER, totalBufferSize *4, normals, GL.GL_STATIC_DRAW);

            // Free the normals
            normals = null;


            // Third create the TEXTURES buffer for each object (if the object
            // has a texture...skip it if it does not)
            //  ...corresponding to the VBO[4*i+2] ids
            if(!group.material.equals("")) {
                // Bind the buffer to represent which buffer we are currently working with
                gl.glBindBuffer(GL.GL_ARRAY_BUFFER, VBO[4*i+2]);

                // Textures have 2 coordinates (u,v) per vertex and 3 vertices per triangle/face
                int totalBuffer = (group.faces.size()*group.facePoints*2);

                // Allocate the float buffer for the textures (again, 4 bytes per float so multiply by 4)
                textures = ByteBuffer.allocateDirect(totalBuffer * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();

                for (int j = 0; j < group.faces.size(); j++) {
                    for (int whichVertex=0; whichVertex<group.faces.get(j).VT.size(); whichVertex++) {
                        if (group.faces.get(j).VT.size() > 0)
                        {
                            Vertex v = group.faces.get(j).VT.get(whichVertex);

                            // put the u,v coordinates of the texture vertices into the textures FloatBuffer
                            textures.put((float) v.x);
                            textures.put((float) v.y);
                        }
                        else
                        {
                            textures.put(0);
                            textures.put(0);
                        }
                    }
                }

                // Rewind buffer
                textures.rewind();

                // Write out the textures buffer to the currently bound VBO.
                // Use 'totalBufferSize*4' because we have 4 bytes for a float.
                gl.glBufferData(GL.GL_ARRAY_BUFFER, totalBuffer*4, textures, GL.GL_STATIC_DRAW);

                // Free the textures
                textures = null;
            }


            // Fourth create the COLORS buffer for each object (if the object has
            // a texture then you don't need to set a color buffer for that object...but
            // make sure not to use the empty buffer during rendering by accident...you
            // could just put in fake colors for the buffer just in case...I don't do that
            // here)
            //  ...corresponding to the VBO[4*i+3] ids
            // Bind the buffer to represent which buffer we are currently working with
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, VBO[4*i+3]);

            // Allocate the float buffer for the colors.  The size of the
            // buffer of normals corresponds to the same number of floats
            // as the vertices.
            totalBufferSize = group.numVerts*3;
            colors = FloatBuffer.allocate(totalBufferSize);

            // Create the float buffer for the colors by looping over each
            // face and each face's vertex and adding the color of
            // each corresponding vertex to the buffer
            for (int j=0; j<group.faces.size(); j++) {
                // if the material id is in the materials list
                for (int z=0; z<group.faces.get(j).V.size()*3; z++) {
                    colors.put(1.0f);
                }
            }

            // Rewind buffer
            colors.rewind();

            // Write out the colors buffer to the currently bound VBO.
            // Use 'totalBufferSize*4' because we have 4 bytes for a float.
            gl.glBufferData(GL.GL_ARRAY_BUFFER, totalBufferSize*4, colors, GL.GL_STATIC_DRAW);

            // Free colors
            colors = null;
        }
    }
}
