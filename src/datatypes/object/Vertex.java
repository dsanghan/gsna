package datatypes.object;

/**
 * User: Dev
 * Date: 5/26/13
 * Time: 10:14 PM
 */
public class Vertex
{
    public double x = 0;
    public double y = 0;
    public double z = 0;

    public Vertex(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String toString()
    {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vertex subtract(Vertex p) {
        return new Vertex(x - p.x, y - p.y, z - p.z);
    }

    public double length()
    {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public void normalize()
    {
        double length = length();
        x /= length;
        y /= length;
        z /= length;
    }
}
