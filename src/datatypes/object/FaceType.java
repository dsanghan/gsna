package datatypes.object;

/**
 * User: Dev
 * Date: 5/28/13
 * Time: 3:53 PM
 */
public enum FaceType
{
    VT, VTN, VN;
}
