package datatypes.object;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import parser.ObjectParserLexer;
import parser.ObjectParserParser;
import utils.Constants;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: Dev
 * Date: 5/26/13
 * Time: 9:48 PM
 */
public class Obj
{
    String name;
    ArrayList<Vertex> V;
    ArrayList<Vertex> VT;
    ArrayList<Vertex> VN;
    ArrayList<Face> faces;
    HashMap<String, Group> groups;
    ArrayList<Group> groupsValues;
    public VertexBuffer vbo;

    private static HashMap<String, Obj> objects;
    public static String activeName = "";
    public static String activeGroup = "";

    public static Obj getObj(String name)
    {
        if (objects == null)
            objects = new HashMap<String, Obj>();

        if (!objects.containsKey(name))
            objects.put(name, new Obj(name));

        return objects.get(name);
    }

    private Obj(String name)
    {
        this.name = name;
        V = new ArrayList<Vertex>();
        VT = new ArrayList<Vertex>();
        VN = new ArrayList<Vertex>();
        faces = new ArrayList<Face>();
        groups = new HashMap<String, Group>();
        groupsValues = new ArrayList<Group>();
    }

    public void addV(Vertex v)
    {
        V.add(v);
    }

    public void addVT(Vertex v)
    {
        VT.add(v);
    }

    public void addVN(Vertex v)
    {
        VN.add(v);
    }

    public void addFace(Face f)
    {
        faces.add(f);
        if (!groups.containsKey(activeGroup))
        {
            groups.put(activeGroup, new Group(activeGroup));
            groupsValues.add(groups.get(activeGroup));
        }

        groups.get(activeGroup).addFace(f);
    }

    public Vertex getV(int index)
    {
        return V.get(index - 1);
    }

    public Vertex getVT(int index)
    {
        return VT.get(index - 1);
    }

    public Vertex getVN(int index)
    {
        return VN.get(index - 1);
    }

    public void printFaces()
    {
        for (Face f : faces)
        {
            System.out.println(f.toString());
        }
    }

    public ArrayList<Face> getFaces() {
        return faces;
    }

    public static void loadObject(String filename)
    {
        activeName = filename;
        try {
            StringBuffer data = new StringBuffer();
            BufferedReader br = new BufferedReader(new FileReader(filename));

            String line;

            while ((line = br.readLine()) != null)
                data.append(line+"\n");
            data.append("\n");

            CharStream cs = new ANTLRStringStream(data.toString());
            ObjectParserLexer lexer = new ObjectParserLexer(cs);
            CommonTokenStream tokenStream = new CommonTokenStream(lexer);
            ObjectParserParser parser = new ObjectParserParser(tokenStream);
            parser.start();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return;
    }

    public ArrayList<Group> getGroups()
    {
        return groupsValues;
    }
}
