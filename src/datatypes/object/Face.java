package datatypes.object;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 5/28/13
 * Time: 3:52 PM
 */
public class Face
{
    FaceType type;
    public static String material_name = "";

    public ArrayList<Vertex> V;
    public ArrayList<Vertex> VT;
    public ArrayList<Vertex> VN;

    public String material;

    public Face()
    {
        V = new ArrayList<Vertex>();
        VT = new ArrayList<Vertex>();
        VN = new ArrayList<Vertex>();
        this.material = new String(material_name);
    }

    public Face(FaceType faceType)
    {
        type = faceType;
        V = new ArrayList<Vertex>();
        VT = new ArrayList<Vertex>();
        VN = new ArrayList<Vertex>();
        this.material = material_name;
    }

    public void addV(Vertex v)
    {
        V.add(v);
    }

    public void addVT(Vertex v)
    {
        VT.add(v);
    }

    public void addVN(Vertex v)
    {
        VN.add(v);
    }

    public FaceType getType()
    {
        return type;
    }

    public String toString()
    {
        String output = "face: ";

        for (int i = 0; i < V.size(); i++)
        {
            Vertex v = V.get(i);
            output += "\n" + v.toString();
            if (VT.size() > 0)
            {
                Vertex v2 = VT.get(i);
                output += "\t" + v2.toString();
            }
            if (VN.size() > 0)
            {
                Vertex vn = VN.get(i);
                output += "\t" + vn.toString();
            }
        }

        return output;
    }

    public ArrayList<Vertex> getV() {
        return V;
    }

    public ArrayList<Vertex> getVN()
    {
        return VN;
    }

    public ArrayList<Vertex> getVT() {
        return VT;
    }
}
