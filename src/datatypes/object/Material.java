package datatypes.object;

import com.jogamp.opengl.util.texture.Texture;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import parser.MTLParserLexer;
import parser.MTLParserParser;
import utils.TextureTools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

/**
 * User: Dev
 * Date: 5/28/13
 * Time: 11:29 PM
 */
public class Material
{
    private static HashMap<String, Material> materials;
    public float optical_density;
    public float specular_exponent;
    public float dissolve;
    public float tr;
    public float transmission_filter[];
    public float illumination;
    public float ambient_color[];
    public float diffuse_color[];
    public float specular_color[];
    public float emission_color[];
    public String ambient_path;
    public String diffuse_path;
    public String specular_path;
    public String emission_path;
    public String bump_path;
    public Texture diffuseTexture;

    public static Material activeMaterial = null;

    public static Material getMaterial(String name)
    {
        if (materials == null)
        {
            materials = new HashMap<String, Material>();
        }

        if (!materials.containsKey(name))
        {
            materials.put(name, new Material());
        }

        return materials.get(name);
    }

    private void Material()
    {
        ambient_color = null;
        diffuse_color = null;
        specular_color = null;
        emission_color = null;
    }

    public void loadMaterial(String filename)
    {
        diffuseTexture = TextureTools.loadTexture(filename);
    }

    public static void loadMaterials()
    {
        for (String key : materials.keySet())
        {
            Material mat = materials.get(key);
            if (mat.diffuse_path != null)
                mat.loadMaterial(mat.diffuse_path);
        }

        return;
    }

    public static void parseMTLFile(String filename)
    {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuffer data = new StringBuffer();
            String line;

            while ((line = br.readLine()) != null)
            {
                data.append(line+"\n");
            }

            data.append("\n");

            CharStream cs = new ANTLRStringStream(data.toString());
            MTLParserLexer lexer = new MTLParserLexer(cs);
            CommonTokenStream tokenStream = new CommonTokenStream(lexer);
            MTLParserParser parser = new MTLParserParser(tokenStream);
            parser.start();

        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
