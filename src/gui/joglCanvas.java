package gui;

import java.awt.*;
import java.awt.Point;
import java.awt.event.*;
import javax.media.opengl.*;
import javax.swing.*;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import datatypes.arcball.ArcBall;
import datatypes.arcball.Matrix4f;
import datatypes.arcball.Quat4f;
import datatypes.object.Material;
import datatypes.object.Obj;
import datatypes.object.VertexBuffer;
import datatypes.simulator.Workspace;
import utils.Constants;
import utils.GLTools;
import utils.SetupTools;

import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants

/**
 * JOGL 2.0 Program Template (GLCanvas)
 * This is a "Component" which can be added into a top-level "Container".
 * It also handles the OpenGL events to render graphics.
 */
@SuppressWarnings("serial")
public class JoglCanvas extends GLCanvas implements GLEventListener {
    // Define constants for the top-level container
    private static String TITLE = "JOGL 2.0 Setup (GLCanvas)";  // window's title
    private static final int CANVAS_WIDTH = 1800;  // width of the drawable
    private static final int CANVAS_HEIGHT = 900; // height of the drawable
    private static final int FPS = 60; // animator's target frames per second

    private static double xInit;
    private static double yInit;
    private static double xCurr;
    private static double yCurr;
    private static int mouseMode;

    private static boolean shift = false;

    private static double[] cameraPos = new double[] {7.5f, 5f, 30f};

    private static Workspace w;

    /** The entry main() method to setup the top-level container and animator */
    public static void main(String[] args) {

        Obj.loadObject(Constants.objName);
        Obj.loadObject(Constants.goalName);

        // Run the GUI codes in the event-dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Create the OpenGL rendering canvas
                GLCanvas canvas = new JoglCanvas();
                canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));

                canvas.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e)
                    {
                        keyPressedFunc(e);
                    }

                    public void keyReleased(KeyEvent e)
                    {
                        keyReleaseFunc(e);
                    }
                });

                canvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e)
                    {
                        mousePressedFunc(e);
                    }

                    public void mouseReleased(MouseEvent e)
                    {
                        mouseReleasedFunc(e);
                    }
                });

                canvas.addMouseMotionListener(new MouseMotionAdapter() {
                    @Override
                    public void mouseDragged(MouseEvent e)
                    {
                        mouseMotionFunc(e);
                    }
                });

                // Create a animator that drives canvas' display() at the specified FPS.
                final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);

                // Create the top-level container
                final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
                frame.getContentPane().add(canvas);
                frame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        // Use a dedicate thread to run the stop() to ensure that the
                        // animator stops before program exits.
                        new Thread() {
                            @Override
                            public void run() {
                                if (animator.isStarted()) animator.stop();
                                System.exit(0);
                            }
                        }.start();
                    }
                });
                frame.setTitle(TITLE);
                frame.pack();
                frame.setVisible(true);
                animator.start(); // start the animation loop
            }
        });
    }

    // Setup OpenGL Graphics Renderer

    private GLU glu;  // for the GL Utility
    private double time;
    private Texture robotTexture;
    private Matrix4f LastRot = new Matrix4f();
    private Matrix4f ThisRot = new Matrix4f();
    private ArcBall arcBall = new ArcBall(CANVAS_WIDTH, CANVAS_HEIGHT);

    /** Constructor to setup the GUI for this Component */
    public JoglCanvas() {
        this.addGLEventListener(this);
        time = 0;
    }

    // ------ Implement methods declared in GLEventListener ------

    /**
     * Called back immediately after the OpenGL context is initialized. Can be used
     * to perform one-time initialization. Run only once.
     */
    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
        glu = new GLU();                         // get GL Utilities
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
        gl.glClearDepth(1.0f);      // set clear depth value to farthest
        gl.glEnable(GL_DEPTH_TEST); // enables depth testing
        gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective correction
        gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting
        //gl.glEnable(GL2.GL_LINE_SMOOTH);
        //gl.glEnable(GL2.GL_POLYGON_SMOOTH);
        //gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST);

        w = SetupTools.setupWorkspace();
        // ----- Your OpenGL initialization code here -----

        initLights(gl);

        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_GENERATE_MIPMAP, GL2.GL_TRUE);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAX_LEVEL, 5);

        Material mat = Material.getMaterial("obstacle");
        mat.diffuse_path = Constants.obstacleMaterialPath;
        mat = Material.getMaterial("workspace");
        mat.diffuse_path = Constants.workspaceMaterialPath;
        Material.loadMaterials();

        Obj.getObj(Constants.objName).vbo = new VertexBuffer(gl, Obj.getObj(Constants.objName).getGroups());
        return;

    }

    public void initLights(GL2 gl)
    {
        float SHINE_ALL_DIRECTIONS = 1;

        float[] lightPos = {30, 0, 0, SHINE_ALL_DIRECTIONS};
        float[] lightColorAmbient = {0.4f, 0.4f, 0.4f, 0.4f};
        float[] lightColorDiffuse = {3f, 3f, 3f, 3f};
        float[] lightColorSpecular = {0.8f, 0.8f, 0.8f, 1f};

        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, lightColorAmbient, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, lightColorDiffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, lightColorSpecular, 0);

        enableLights(gl);

    }

    public void enableLights(GL2 gl)
    {
        gl.glEnable(GL2.GL_LIGHT0);
        gl.glEnable(GL2.GL_LIGHTING);
    }

    public void disableLights(GL2 gl)
    {
        gl.glDisable(GL2.GL_LIGHT0);
        gl.glDisable(GL2.GL_LIGHTING);
    }

    /**
     * Call-back handler for window re-size event. Also called when the drawable is
     * first set to visible.
     */
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context

        if (height == 0) height = 1;   // prevent divide by zero
        float aspect = (float)width / height;

        // Set the view port (display area) to cover the entire window
        gl.glViewport(0, 0, width, height);

        // Setup perspective projection, with aspect ratio matches viewport
        gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
        gl.glLoadIdentity();             // reset projection matrix
        glu.gluPerspective(45.0, aspect, 0.1, 500.0); // fovy, aspect, zNear, zFar

        // Enable the model-view transform
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity(); // reset
        gl.glTranslated(-cameraPos[0], -cameraPos[1], -cameraPos[2]);
        //gl.glRotatef(30, 0, 1, 1);
        arcBall.setBounds((float) width, (float) height);
    }

    void reset() {
            LastRot.setIdentity();   // Reset Rotation
            ThisRot.setIdentity();   // Reset Rotation
    }

    void startDrag( Point MousePt ) {
        LastRot.set( ThisRot );  // Set Last Static Rotation To Last Dynamic One
        arcBall.click( MousePt );    // Update Start Vector And Prepare For Dragging
    }

    void drag( Point MousePt, GL2 gl)       // Perform Motion Updates Here
    {
        Quat4f ThisQuat = new Quat4f();

        // Update End Vector And Get Rotation As Quaternion
        arcBall.drag( MousePt, ThisQuat);
        gl.glRotated(ThisQuat.w, ThisQuat.x, ThisQuat.y, ThisQuat.z);
        ThisRot.setRotation(ThisQuat);  // Convert Quaternion Into Matrix3fT
        ThisRot.mul( ThisRot, LastRot); // Accumulate Last Rotation Into This One
    }

    /**
     * Called back by the animator to perform rendering.
     */
    @Override
    public void display(GLAutoDrawable drawable) {
        w.tick();
        GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
        //gl.glClearColor(0.3f, 0.3f, 0.3f, 0.3f);

        if (Constants.changelighting)
        {
            if (Constants.lighting = true)
                enableLights(gl);
            else
                disableLights(gl);
            Constants.changelighting = false;
        }

        if (mouseMode == 3)
        {
            gl.glTranslated((xCurr - xInit) / 50.0, (yInit - yCurr) / 50.0, 0);
            cameraPos[0] -= (xCurr - xInit) / 50.0;
            cameraPos[1] -= (yInit - yCurr) / 50.0;
        }

        else if (mouseMode == 2)
            gl.glTranslated(0f, 0f, (yInit - yCurr));

        else if (mouseMode == 1)
        {
            if (!isdragging)
                startDrag(new Point((int)xInit, (int)yInit));
            else
            {
                drag(new Point((int)xCurr, (int)yCurr), gl);
                xInit = xCurr;
                yInit = yCurr;
            }
        }

        gl.glPushMatrix();

        xInit = xCurr;
        yInit = yCurr;

        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers

        gl.glDisable(GL_LIGHTING);
        GLTools.renderWorkspace(gl, w);
        gl.glEnable(GL_LIGHTING);


        if (Constants.useVBO)
        {
            Obj obj = Obj.getObj(Constants.objName);
            GLTools.renderVBO(gl, obj.vbo, obj.getGroups(), w.getRobot());
        }
        else
        {
            GLTools.renderObject(gl, Obj.getObj(Constants.objName), w.getRobot());
        }
        GLTools.renderGoal(gl, Obj.getObj(Constants.goalName), w.getRobot().goal);

        gl.glPopMatrix();
    }

    /**
     * Called back before the OpenGL context is destroyed. Release resource such as buffers.
     */
    @Override
    public void dispose(GLAutoDrawable drawable) { }

    public static boolean isdragging = false;

    public static void mousePressedFunc(MouseEvent e)
    {
        if (!shift)
        {
            mouseMode = e.getButton();
            xInit = e.getX();
            yInit = e.getY();
            xCurr = e.getX();
            yCurr = e.getY();
        }
        else
        {
            w.getRobot().goal = new datatypes.simulator.Point(e.getX(), e.getY());
        }
    }

    public static void mouseReleasedFunc(MouseEvent e)
    {
        mouseMode = 0;
        isdragging = false;
    }

    public static void mouseMotionFunc(MouseEvent e)
    {
        if (mouseMode == 1 || mouseMode == 2 || mouseMode == 3)
        {
            xCurr = e.getX();
            yCurr = e.getY();
            isdragging = true;
        }
    }

    public static void keyPressedFunc(KeyEvent e)
    {
        if (e.isShiftDown())
            shift = true;
        else
            shift = false;

        switch (e.getKeyChar())
        {
            case 'l':
                Constants.lighting = !Constants.lighting;
                Constants.changelighting = true;
                break;
            case 'h':
                Constants.showObjects = !Constants.showObjects;
                break;
            case 'w':
                Constants.drawWireFrame = !Constants.drawWireFrame;
                break;
            case 'r':
                Constants.useVBO = !Constants.useVBO;
                System.out.println("Using VBO: " + Constants.useVBO);
                break;
            case 'p':
                Constants.pause = !Constants.pause;
                break;
            default:
                break;
        }
    }

    public static void keyReleaseFunc(KeyEvent e)
    {

    }
}