package utils;

public class Constants
{
    public static final boolean doLog = true;
    public static final double orientationDistance = 0.5;
    public static final double visibility = 2;
    public static final double robotPointSize = 0.2;
    public static int robotResolution = 720;
    public static int viewAngle = 360;
    public static double thetaIncrement = (double)viewAngle / (double)robotResolution;
    public static double maxTurnRate = 0.5;
    public static double thresholdDistance = 0.02;
    public static double minDistanceX = 1;
    public static double minDistanceY = 1;
    public static double boundaryFollowDistance = 1;
    public static boolean drawWireFrame = false;
    public static boolean lighting = true;
    public static boolean changelighting = false;
    //public static String objName = "Aston_DB9.obj";
    public static String objName = "young_boy_head_obj.obj";
    public static double scale = 5;
    public static boolean useMaterialData = false;
    public static boolean showObjects = true;
    public static boolean useVBO = true;
    public static boolean pause = true;

    public static double BASE_HEIGHT = 0;
    public static double OBJ_HEIGHT = 2;
    public static String obstacleMaterialPath = "res/obstacle.jpg";
    public static String obstacleMaterial = "obstacle";
    public static String workspaceMaterialPath = "res/workspace.jpg";
    public static String workspaceMaterial = "workspace";
    public static String goalName = "Flag_American_Standing.obj";
}
