package utils;

import java.util.HashMap;

public class Logger {
    private static HashMap<String, Logger> loggers;
    public static boolean doLog = Constants.doLog;
    private String name;

    public static enum LogLevel
    {
        INFO, DEBUG, WARNING, FATAL;
    }

    private Logger(String name)
    {
        this.name = name;
    }

    public static Logger getLogger(String name)
    {
        if (loggers == null)
        {
            loggers = new HashMap<String, Logger>();
        }

        if (loggers.containsKey(name))
            return loggers.get(name);

        Logger logger = new Logger(name);
        loggers.put(name, logger);
        return logger;
    }

    public void log(LogLevel level, String desc)
    {
        if (doLog)
            System.out.println(name + " : " + level.toString() + " : " + desc);
    }

    public static String timeString(long time)
    {
        long ns = time % 1000000;
        time /= 1000000;
        String nano = String.valueOf(ns);
        while (nano.length() != 6)
            nano = "0" + nano;
        long s = time % 60;
        time /= 60;
        long m = time % 60;
        time /= 60;
        long hours = time;

        return String.valueOf(hours) + ":" + String.valueOf(m) + ":" +
               String.valueOf(s) + "." + nano;
    }
}
