package utils;

import datatypes.simulator.Obstacle;
import datatypes.simulator.Point;
import datatypes.simulator.Robot;
import datatypes.simulator.Workspace;

import java.util.ArrayList;

/**
 * User: Dev
 * Date: 5/17/13
 * Time: 3:33 PM
 */
public class SetupTools
{
    private static Logger logger = Logger.getLogger("Setup Tools");

    public static Workspace setupWorkspace()
    {
        logger.log(Logger.LogLevel.INFO, "Setting up obstacle 1...");
        ArrayList<Point> obstacle1 = new ArrayList<Point>();
        obstacle1.add(new Point(4, 5));
        obstacle1.add(new Point(4, 6));
        obstacle1.add(new Point(6, 6));
        obstacle1.add(new Point(6, 14));
        obstacle1.add(new Point(4, 14));
        obstacle1.add(new Point(4, 15));
        obstacle1.add(new Point(8, 15));
        obstacle1.add(new Point(8, 5));
        //Obstacle o1 = new Obstacle(obstacle1);

        logger.log(Logger.LogLevel.INFO, "Setting up obstacle 2...");
        ArrayList<Point> obstacle2 = new ArrayList<Point>();
        ArrayList<Point> renderPoints = new ArrayList<Point>();
        renderPoints.add(new Point(10, 6));
        renderPoints.add(new Point(10, 14));
        renderPoints.add(new Point(14, 14));
        renderPoints.add(new Point(14, 13));
        renderPoints.add(new Point(12, 13));
        renderPoints.add(new Point(12, 7));
        renderPoints.add(new Point(14, 7));
        renderPoints.add(new Point(14, 6));
        obstacle2.add(new Point(9, 5));
        obstacle2.add(new Point(9, 15));
        obstacle2.add(new Point(15, 15));
        obstacle2.add(new Point(15, 12));
        obstacle2.add(new Point(13, 12));
        obstacle2.add(new Point(13, 8));
        obstacle2.add(new Point(15, 8));
        obstacle2.add(new Point(15, 5));
        Obstacle o2 = new Obstacle(obstacle2, renderPoints);


        ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
        //obstacles.add(o1);
        obstacles.add(o2);

        logger.log(Logger.LogLevel.INFO, "Setting up workspace...");
        ArrayList<Point> workspace = new ArrayList<Point>();
        /*
        workspace.add(new datatypes.simulator.Point(0.0,0.0));
        workspace.add(new datatypes.simulator.Point(0.0,5.0));
        workspace.add(new datatypes.simulator.Point(10.0,5.0));
        workspace.add(new datatypes.simulator.Point(10.0,10.0));
        workspace.add(new datatypes.simulator.Point(15.0,10.0));
        workspace.add(new datatypes.simulator.Point(15.0,0.0));
        */

        workspace.add(new Point(0.0, 0.0));
        workspace.add(new Point(0.0, 20.0));
        workspace.add(new Point(20.0, 20.0));
        workspace.add(new Point(20.0, 0.0));
        Workspace w = new Workspace(workspace);

        logger.log(Logger.LogLevel.INFO, "Setting up Robot...");
        Robot r1 = new Robot(4, 10.0);
        r1.setGoal(16, 10);

        //w.addObstacle(o1);
        w.addObstacle(o2);
        w.setRobot(r1);

        return w;
    }
}
