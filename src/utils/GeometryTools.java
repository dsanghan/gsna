package utils;

import datatypes.simulator.Edge;
import datatypes.simulator.Node;
import datatypes.simulator.Obstacle;
import datatypes.simulator.Point;
import datatypes.object.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * User: Dev
 * Date: 4/27/13
 * Time: 3:06 PM
 */
public class GeometryTools
{
    public static boolean insideAnyObstacle(Point point, HashSet<Obstacle> obstacles)
    {
        for (Obstacle o : obstacles)
        {
            if (o.inRange(point))
                if (GeometryTools.insideObstacle(point, o))
                    return true;

        }

        return false;
    }

    public static void findAllPaths(Point start,
                                    Point end,
                                    HashMap<String, Node> visGraph,
                                    ArrayList<ArrayList<Node>> allPaths,
                                    ArrayList<Node> currentPath)
    {

    }

    /**
     * Implements the rayCheck algorithm to find if a point is inside an obstacle or not.
     * @param point
     * @param obstacle
     * @return
     */
    public static boolean insideObstacle(Point point, Obstacle obstacle)
    {
        if (!obstacle.inRange(point))
            return false;

        Edge rayCheck = new Edge(point, new Point(point.getX() + 3 * obstacle.maxDistance, point.getY()), null);

        int numIntersects = 0;

        for (Edge e : obstacle.getEdges())
        {
            if (GeometryTools.intersects(e, rayCheck))
                numIntersects++;
        }

        return numIntersects % 2 != 0;
    }

    public static ArrayList<Node> DFS(Point start, Point end, HashMap<String, Node> visGraph)
    {
        ArrayList<Node> visitQueue = new ArrayList<Node>();
        ArrayList<Node> path = new ArrayList<Node>();

        visitQueue.add(visGraph.get(start.toString()));

        while (visitQueue.size() != 0)
        {
            Node currNode = visitQueue.remove(0);

            if (currNode.visited)
                continue;

            currNode.visited = true;

            int newLength = currNode.pathLength + 1;

            for (Node node : currNode.connections)
            {
                if (node.visited)
                {
                    if (newLength < node.pathLength)
                    {
                        node.previous = currNode;
                        node.pathLength = currNode.pathLength + 1;
                    }

                    continue;
                }

                if (node.previous == null || newLength < node.pathLength)
                {
                    node.previous = currNode;
                    node.pathLength = currNode.pathLength + 1;
                }

                visitQueue.add(node);
            }
        }

        Node goal = visGraph.get(end.toString());

        ArrayList<Node> finalPath = new ArrayList<Node>();

        while (goal != null)
        {
            finalPath.add(0, goal);
            goal = goal.previous;
        }

        return finalPath;
    }

    public static HashMap<String, Node> constructTree(ArrayList<Edge> allEdges, Point head, Point tail)
    {
        HashMap<String, Node> visibilityGraph = new HashMap<String, Node>();

        for (Edge e : allEdges)
        {
            Point p1 = e.p1;
            Point p2 = e.p2;

            Node n1;
            Node n2;

            if (visibilityGraph.containsKey(p1.toString()))
                n1 = visibilityGraph.get(p1.toString());

            else
            {
                n1 = new Node(e.p1.getX(), e.p1.getY());
                visibilityGraph.put(p1.toString(), n1);
            }

            if (visibilityGraph.containsKey(p2.toString()))
                n2 = visibilityGraph.get(p2.toString());

            else
            {
                n2 = new Node(e.p2.getX(), e.p2.getY());
                visibilityGraph.put(p2.toString(), n2);
            }

            n1.connections.add(n2);
            n2.connections.add(n1);
        }

        return visibilityGraph;
    }

    public static ArrayList<Edge> filterEdges(ArrayList<Edge> input, Obstacle workspace)
    {
        ArrayList<Edge> result = new ArrayList<Edge>();

        for (Edge e : input)
        {
            boolean add = true;

            for (Edge w : workspace.getEdges())
            {
                if (intersects(e,w))
                    add = false;
            }

            if (add)
                result.add(e);
        }

        return result;
    }

    public static double[][] convertEdgeListToDouble(ArrayList<Edge> edges)
    {
        double result[][] = new double[edges.size() * 2][2];

        for (int i = 0; i < edges.size(); i+=2)
        {
            Edge temp = edges.get(i);

            result[i][0] = temp.p1.getX();
            result[i][1] = temp.p1.getY();
            result[i+1][0] = temp.p2.getX();
            result[i+1][1] = temp.p2.getY();
        }

        return result;
    }

    public static boolean intersects(Edge e1, Edge e2)
    {
        Point P = e1.p1;
        Point Q = e1.p2;

        Point E = e2.p1;
        Point F = e2.p2;

        double CP1 = ((F.getX() - E.getX())*(P.getY() - F.getY())) -
                     ((F.getY() - E.getY())*(P.getX() - F.getX()));

        double CP2 = ((F.getX() - E.getX())*(Q.getY() - F.getY())) -
                ((F.getY() - E.getY())*(Q.getX() - F.getX()));

        P = e2.p1;
        Q = e2.p2;

        E = e1.p1;
        F = e1.p2;

        double CP3 = ((F.getX() - E.getX())*(P.getY() - F.getY())) -
                ((F.getY() - E.getY())*(P.getX() - F.getX()));

        double CP4 = ((F.getX() - E.getX())*(Q.getY() - F.getY())) -
                ((F.getY() - E.getY())*(Q.getX() - F.getX()));

        if (CP2 == 0 || CP4 == 0)
            return false;

        if (CP1 / CP2 < 0 && CP3 / CP4 < 0)
            return true;

        return false;
    }

    public static Point getIntersectionPoint(Point p0, Point p1, Point p2, Point p3)
    {
        double s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, t_numer, denom, t;

        double p0_x = p0.getX();
        double p0_y = p0.getY();
        double p1_x = p1.getX();
        double p1_y = p1.getY();
        double p2_x = p2.getX();
        double p2_y = p2.getY();
        double p3_x = p3.getX();
        double p3_y = p3.getY();

        s10_x = p1_x - p0_x;
        s10_y = p1_y - p0_y;
        s32_x = p3_x - p2_x;
        s32_y = p3_y - p2_y;

        denom = s10_x * s32_y - s32_x * s10_y;
                s02_x = p0_x - p2_x;
        s02_y = p0_y - p2_y;

        t_numer = s32_x * s02_y - s32_y * s02_x;

        t = t_numer / denom;

        return new Point(p0_x + (t * s10_x), p0_y + (t * s10_y));
    }

    public static ArrayList<Edge> findTangentsBetweenObstacles(Obstacle o1, Obstacle o2)
    {
        ArrayList<Edge> result = new ArrayList<Edge>();

        boolean add = true;

        for (Point p1 : o1.getPoints())
        {
            ArrayList<Edge> newTangents = findTangentsToObstacle(p1, o2);
            Edge tangent1 = newTangents.get(0);
            Edge tangent2 = newTangents.get(1);

            for (Edge e : o1.getEdges())
            {
                if (intersects(e, tangent1) || intersects(e, tangent2))
                    add = false;
            }

            if (add)
            {
                result.add(tangent1);
                result.add(tangent2);
            }

            add = true;
        }

        return result;
    }

    public static double calculateAngleBetweenVectors(Point p1, Point p2, Point p3)
    {
        Point vec1 = p1.subtract(p2);
        Point vec2 = p3.subtract(p2);

        double angle = vec1.dot(vec2) / (vec1.length() * vec2.length());

        return Math.acos(angle) * 180 / Math.PI;
    }

    public static boolean intersectsObstacle(Obstacle o, Edge e)
    {
        for (Edge oe : o.getEdges())
        {
            if (GeometryTools.intersects(e, oe))
                return true;
        }

        return false;
    }

    public static ArrayList<Edge> findTangentsToObstacle(Point point, Obstacle obstacle)
    {
        Point tangent1 = new Point(0, 0);
        Point tangent2 = new Point(0, 0);

        double maxAngle = 0;

        ArrayList<Point> obstaclePoints = obstacle.getTangentPoints();

        for (Point p1 : obstaclePoints)
        {
            for (Point p2 : obstaclePoints)
            {
                double angle = calculateAngleBetweenVectors(p1, point, p2);

                if (angle > maxAngle)
                {
                    maxAngle = angle;
                    tangent1 = p1;
                    tangent2 = p2;
                }

            }
        }


        ArrayList<Edge> result = new ArrayList<Edge>();

        result.add(new Edge(point, tangent1, obstacle));
        result.add(new Edge(point, tangent2, obstacle));

        return result;
    }

    public static ArrayList<Point> jarvisMarch(ArrayList<Point> points)
    {
        ArrayList<Point> result = new ArrayList<Point>();
        Point leftmost = points.get(0);
        int left = 0;

        for (int i = 0; i < points.size(); i++)
        {
            Point currPoint = points.get(i);

            if ((currPoint.getX() < leftmost.getX()) ||
                (currPoint.getX() == leftmost.getX() && currPoint.getY() > leftmost.getY()))
            {
                leftmost = currPoint;
                left = i;
            }
        }

        Point firstPoint = leftmost;
        Point point = leftmost;
        points.set(left, points.get(0));
        points.set(0, firstPoint);

        Point prevPoint = new Point(leftmost.getX() + 100, leftmost.getY());

        result.add(firstPoint);

        for (int i = 0; i < points.size(); i++)
        {
            double maxAngle = 0;
            Point maxPoint = null;
            int maxJ = 0;

            for (int j = 0; j < points.size(); j++)
            {
                Point currPoint = points.get(j);
                double angle = calculateAngleBetweenVectors(prevPoint, point, currPoint);

                if (angle > maxAngle)
                {
                    maxJ = j;
                    maxAngle = angle;
                    maxPoint = currPoint;
                }
                else if (angle == maxAngle && maxPoint != null)
                {
                    double distance1 = point.distanceTo(maxPoint);
                    double distance2 = point.distanceTo(currPoint);

                    if (distance2 > distance1)
                    {
                        maxJ = j;
                        maxAngle = angle;
                        maxPoint = currPoint;
                    }
                }
            }

            if (maxPoint == null || maxPoint.equals(firstPoint))
                break;

            result.add(maxPoint);
            Point temp = points.get(i);
            points.set(i, maxPoint);
            points.set(maxJ, temp);
            prevPoint = point;
            point = maxPoint;
        }

        return result;
    }

    public static Vertex crossProduct(Vertex p1, Vertex p2, Vertex p3)
    {
        Vertex v1 = p2.subtract(p1);
        Vertex v2 = p3.subtract(p1);

        return new Vertex(v1.y * v2.z - v1.z * v2.y,
                          v1.z * v2.x - v1.x * v2.z,
                          v1.x * v2.y - v1.y * v2.z);
    }
}
