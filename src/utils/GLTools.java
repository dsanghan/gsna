package utils;

import com.jogamp.opengl.util.texture.Texture;
import datatypes.object.*;
import datatypes.simulator.*;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import java.util.ArrayList;

import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;

/**
 * User: Dev
 * Date: 5/16/13
 * Time: 9:25 PM
 */
public class GLTools
{
    public static void renderWorkspace(GL2 gl, Workspace w)
    {
        if (!Constants.showObjects)
        {
            gl.glBegin(GL2.GL_LINE_LOOP);
            gl.glColor3f(1f, 1f, 1f);

            for (Point point : w.getPoints())
            {
                gl.glVertex3d(point.getX(), point.getY(), 0);
            }

            gl.glEnd();

            for (Obstacle obstacle : w.getObstacleMap())
                renderObstacle(gl, obstacle);

            renderRobot(gl, w.getRobot());
        }

        /*
        for (Obstacle obstacle : w.getObstacleMap())
        {
            if (obstacle.inRange(w.getRobot().position))
            {
                ArrayList<Edge> tangents = GeometryTools.findTangentsToObstacle(w.getRobot().position, obstacle);
                for (Edge tangent : tangents)
                {
                    gl.glBegin(GL.GL_LINES);
                    gl.glColor3f(0f, 0f, 1f);
                    gl.glVertex3d(tangent.p1.getX(), tangent.p1.getY(), 0);
                    gl.glVertex3d(tangent.p2.getX(), tangent.p2.getY(), 0);
                    gl.glEnd();
                }
            }
        }*/

        gl.glEnable(GL_LIGHTING);
        for (Obstacle obstacle : w.getObstacleMap())
        {
            GLTools.renderObject(gl, obstacle.obsObject, null);
        }

        gl.glColor3f(1f, 1f, 1f);
        GLTools.renderObject(gl, w.workspaceObj, null);

        gl.glDisable(GL_LIGHTING);
    }

    public static void renderObstacle(GL2 gl, Obstacle obstacle)
    {
        gl.glBegin(GL2.GL_LINE_LOOP);
        gl.glColor3f(1f, 0, 0);

        for (Point point : obstacle.getPoints())
        {
            gl.glVertex3d(point.getX(), point.getY(), 0);
        }

        gl.glEnd();

        gl.glColor3f(1f, 1f, 1f);
    }

    public static Face generateFace(Vertex v1, Vertex v2, Vertex v3, Vertex v4, String material)
    {
        Face f = new Face();
        f.material = material;
        f.addV(v1);
        f.addV(v2);
        f.addV(v3);
        f.addV(v4);

        Vertex vt1 = new Vertex(0, 0, 0);
        Vertex vt2 = new Vertex(1, 0, 0);
        Vertex vt3 = new Vertex(1, 1, 0);
        Vertex vt4 = new Vertex(0, 1, 0);

        f.addVT(vt1);
        f.addVT(vt2);
        f.addVT(vt3);
        f.addVT(vt4);

        Vertex normal = GeometryTools.crossProduct(v3, v2, v1);

        normal.normalize();

        f.addVN(normal);
        f.addVN(normal);
        f.addVN(normal);
        f.addVN(normal);
        return f;
    }

    public static void renderGoal(GL2 gl, Obj obj, Point goal)
    {
        gl.glTranslated(goal.getX(), goal.getY(), 0);
        gl.glRotatef(90, 1, 0, 0);
        renderObject(gl, obj, null);
        gl.glTranslated(-goal.getX(), -goal.getY(), 0);
    }

    public static void renderObject(GL2 gl, Obj obj, Robot robot)
    {
        if (!Constants.showObjects)
            return;

        gl.glColor3f(1f, 1f, 1f);

        gl.glPushMatrix();
        double scale = robot == null ? 1.0 : Constants.scale;
        if (robot != null)
        {
            gl.glTranslated(robot.x, robot.y, 0);
            gl.glRotatef(90, 1, 0, 0);
            gl.glRotated(robot.orientation + 90, 0, 1, 0);
        }
        if (Constants.drawWireFrame)
            for (Face face : obj.getFaces())
            {
                gl.glBegin(GL.GL_LINE_LOOP);
                gl.glColor3f(1, 1, 1);
                for (Vertex v : face.getV())
                    gl.glVertex3d(v.x/scale, v.y/scale, v.z/scale);
                gl.glEnd();
            }

        else
        {
            for (Face face : obj.getFaces())
            {
                //System.out.println(face.material);
                ArrayList<Vertex> vs = face.getV();
                ArrayList<Vertex> vn = face.getVN();
                ArrayList<Vertex> vt = face.getVT();

                gl.glColor3f(1f, 1f, 1f);

                Material material = Material.getMaterial(face.material);
                Texture texture = material != null ? material.diffuseTexture : null;

                if (material != null && Constants.useMaterialData)
                {
                    if (material.ambient_color != null ) gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, material.ambient_color, 1);
                    if (material.diffuse_color != null ) gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, material.diffuse_color, 1);
                    if (material.specular_color != null ) gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, material.specular_color, 1);
                    if (material.emission_color != null ) gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, material.emission_color, 0);
                }

                int first = 0;
                int second = 1;

                if (vt.size() != 0)
                {
                    if (texture != null)
                    {
                        texture.enable(gl);
                        texture.bind(gl);
                    }
                }

                gl.glBegin(GL2.GL_TRIANGLE_FAN);

                for (int curr = 0; curr < vs.size(); curr++)
                {
                    Vertex v1 = vs.get(curr);
                    Vertex vn1 = vn.size() > 0 ? vn.get(curr) : null;
                    Vertex vt1 = vt.size() > 0 ? vt.get(curr) : null;

                    if (vt1 != null) gl.glTexCoord3d(vt1.x, vt1.y, vt1.z);
                    if (vn1 != null) gl.glNormal3d(vn1.x, vn1.y, vn1.z);
                    gl.glVertex3d(v1.x/scale, v1.y/scale, v1.z/scale);
                }

                Vertex v1 = vs.get(first);
                Vertex vn1 = vn.size() > 0 ? vn.get(first) : null;
                Vertex vt1 = vt.size() > 0 ? vt.get(first) : null;

                if (vt1 != null) gl.glTexCoord3d(vt1.x, vt1.y, vt1.z);
                if (vn1 != null) gl.glNormal3d(vn1.x, vn1.y, vn1.z);
                gl.glVertex3d(v1.x/scale, v1.y/scale, v1.z/scale);

                gl.glEnd();

                if (texture != null)
                {
                    texture.disable(gl);
                }
            }
        }

        if (robot != null)
            gl.glTranslated(-robot.x, -robot.y, 0);
        gl.glPopMatrix();
    }

    public static void renderVBO(GL2 gl, VertexBuffer vbo, ArrayList<Group> groups, Robot robot)
    {
        if (!Constants.showObjects)
            return;
        gl.glPushMatrix();
        double scale = robot == null ? 1.0 : Constants.scale;
        if (robot != null)
        {
            gl.glTranslated(robot.x, robot.y, 0);
            gl.glRotatef(90, 1, 0, 0);
            gl.glRotated(robot.orientation + 90, 0, 1, 0);
        }

        // Enable the client states for the normals and vertices since we use these
        // in all cases...if you were not using them then you should not enable the
        // states until you do (I had it crash when I had them enabled unnecessarily)
        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);

        // Loop over all of the objects and bind the buffers for the normals and
        // vertices and draw...
        for (int i=0; i<groups.size(); i++) {
            // Get the current object
            Group group = groups.get(i);
            int numFaceIndices = group.numVerts;

            /////////////////////////////////////////////////////////////////
            // Clear/Initialize the display
            //
            // Clear the color so the previous color won't be used to override
            // anything that you didn't have a color for.
            gl.glColor3f(1.0f, 1.0f, 1.0f);

            // Disable the use of textures so that the previous texture's
            // properties are not used to color the current object that
            // doesn't have a texture.
            gl.glDisable(GL2.GL_TEXTURE_2D);

            // Same as above, disable the color material until it is used
            gl.glDisable(GL2.GL_COLOR_MATERIAL);


            /////////////////////////////////////////////////////////////////
            // Start the rendering for each object here
            //
            // USING textures (if the object has a texture)
            Material material = Material.getMaterial(group.material);

            if(material.diffuseTexture != null && !Constants.drawWireFrame) {
                // Enable the client state for the texture coordinate array
                // and for the textures in general.
                gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
                gl.glEnable(GL2.GL_TEXTURE_2D);

                // I have a Texture array ('texture[]') from which I retrieve
                // the texture object to bind it.
                material.diffuseTexture.bind(gl);
                gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.VBO[4*i+2]);
                gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, 0);

                // ELSE using colors
            } else {
                gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
                gl.glEnable(GL2.GL_COLOR_MATERIAL);
                gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.VBO[4*i+3]);

                // If using a 4 float color (then this '3' has to be a '4')
                gl.glColorPointer(3, GL.GL_FLOAT, 0, 0);
            }

            // Bind the normal buffer to work with
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.VBO[4*i+1]);
            gl.glNormalPointer(GL.GL_FLOAT, 0, 0);

            // Bind the vertex buffer to work with
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo.VBO[4*i]);
            gl.glVertexPointer(3, GL.GL_FLOAT, 0, 0);

            // Draw the contents of the VBO...either filled triangles
            // or triangle outlines (wireframe)
            if (!Constants.drawWireFrame) {
                gl.glDrawArrays(GL2.GL_TRIANGLES, 0, numFaceIndices);
            } else {
                gl.glDrawArrays(GL2.GL_LINE_LOOP, 0, numFaceIndices);
            }

            if(material.diffuseTexture != null) {
                gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
            } else {
                gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
            }
        }

        // Disable the client states for the normals and vertices
        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);

        if (robot != null)
            gl.glTranslated(-robot.x, -robot.y, 0);
        gl.glPopMatrix();
    }

    public static void renderRobot(GL2 gl, Robot robot)
    {
        Point position = new Point(robot.x, robot.y);
        Point newPoint = position.createPointOnCircle(robot.getOrientation(), Constants.orientationDistance);

        // Plot visibility circle
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glColor3f(0f, 1f, 0f);
        for (int i = 0; i < 360; i++)
        {
            double angle = i * Math.PI / 180;
            gl.glVertex3d(robot.x + Constants.visibility * Math.cos(angle), robot.y + Constants.visibility * Math.sin(angle), 0);
        }
        gl.glEnd();


        // Plot laser data
        for (double i = 0, j = 0; i < Constants.viewAngle; i+= Constants.thetaIncrement, j++)
        {
            gl.glBegin(GL.GL_LINES);
            gl.glColor3f(0f, 0.5f, 0f);

            double angle = (i + robot.orientation) * Math.PI / 180;
            gl.glVertex3d(robot.x, robot.y, 0);
            gl.glVertex3d(robot.x + robot.visData[(int)j] * Math.cos(angle), robot.y + robot.visData[(int)j] * Math.sin(angle), 0);
            gl.glEnd();
        }

        // Plot robot body
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glColor3f(1f, 0f, 0.5f);
        for (int i = 0; i < 360; i++)
        {
            double angle = i * Math.PI / 180;
            gl.glVertex3d(robot.x + Constants.robotPointSize * Math.cos(angle), robot.y + Constants.robotPointSize * Math.sin(angle), 0);
        }
        gl.glEnd();

        // Plot goal
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glColor3f(0f, 1f, 0.5f);
        for (int i = 0; i < 360; i++)
        {
            double angle = i * Math.PI / 180;
            gl.glVertex3d(robot.goal.getX() + Constants.robotPointSize * Math.cos(angle), robot.goal.getY() + Constants.robotPointSize * Math.sin(angle), 0);
        }
        gl.glEnd();

        // Plot visible tangents
        for (Edge e : robot.tangents)
        {
            gl.glBegin(GL.GL_LINES);
            gl.glColor3f(0.5f, 0.5f, 0);
            gl.glVertex3d(e.p1.getX(), e.p1.getY(), 0);
            gl.glVertex3d(e.p2.getX(), e.p2.getY(), 0);
            gl.glEnd();
        }

        // Plot current target
        Point currTarget = robot.getTargetPosition();
        if (currTarget != null)
        {
            gl.glBegin(GL.GL_LINES);
            gl.glColor3f(1f, 1f, 1f);
            gl.glVertex3d(currTarget.getX() - 0.2, currTarget.getY() - 0.2, 0);
            gl.glVertex3d(currTarget.getX() + 0.2, currTarget.getY() + 0.2, 0);
            gl.glEnd();

            gl.glBegin(GL.GL_LINES);
            gl.glVertex3d(currTarget.getX() - 0.2, currTarget.getY() + 0.2, 0);
            gl.glVertex3d(currTarget.getX() + 0.2, currTarget.getY() - 0.2, 0);
            gl.glEnd();
        }

        // Plot MinPoint
        if (robot.minPoint != null)
        {
            gl.glBegin(GL.GL_LINES);
            gl.glColor3f(1f, 1f, 1f);
            gl.glVertex3d(robot.minPoint.getX() - 0.2, robot.minPoint.getY() - 0.2, 0);
            gl.glVertex3d(robot.minPoint.getX() + 0.2, robot.minPoint.getY() + 0.2, 0);
            gl.glEnd();

            gl.glBegin(GL.GL_LINES);
            gl.glVertex3d(robot.minPoint.getX() - 0.2, robot.minPoint.getY() + 0.2, 0);
            gl.glVertex3d(robot.minPoint.getX() + 0.2, robot.minPoint.getY() - 0.2, 0);
            gl.glEnd();
        }

        gl.glBegin(GL.GL_LINES);
        gl.glColor3f(0f, 1f, 1f);
        gl.glVertex3d(position.getX(), position.getY(), 0);
        gl.glVertex3d(newPoint.getX(), newPoint.getY(), 0);
        gl.glEnd();

        gl.glColor3f(1f, 1f, 1f);
    }

}
