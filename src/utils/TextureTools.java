package utils;

import java.io.File;
import java.io.IOException;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

public class TextureTools {
    public static Texture loadTexture (String name){
        Texture texture = null;
        try {
            texture = TextureIO.newTexture(new File(name), true);// bool - mipmap.


        } catch (IOException e) {
            System.out.println("Could not open file: " + name);
            System.out.println(e);
        }
        return texture;
    }
}