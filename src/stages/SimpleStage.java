package stages;

import utils.GeometryTools;

/**
 * User: Dev
 * Date: 4/24/13
 * Time: 12:32 PM
 */
public class SimpleStage
{
    public static void main(String args[]) throws Exception
    {
        datatypes.simulator.Point p0 = new datatypes.simulator.Point(0, 0);
        datatypes.simulator.Point p1 = new datatypes.simulator.Point(5, 0);
        datatypes.simulator.Point p2 = new datatypes.simulator.Point(2.5, 2.5);
        datatypes.simulator.Point p3 = new datatypes.simulator.Point(2.5, -2.5);

        System.out.println(GeometryTools.getIntersectionPoint(p0, p1, p2, p3));
        System.out.println(-360 % 720);

        System.exit(0);

        /*

        Plot2DPanel plot = new Plot2DPanel();

        ArrayList<Point> obstacle1 = new ArrayList<Point>();
        obstacle1.add(new Point(8, 4));
        obstacle1.add(new Point(12, 4));
        obstacle1.add(new Point(12, 1));
        obstacle1.add(new Point(8, 1));
        Obstacle o1 = new Obstacle(obstacle1);

        ArrayList<Point> obstacle2 = new ArrayList<Point>();
        obstacle2.add(new Point(11, 5));
        obstacle2.add(new Point(13, 5));
        obstacle2.add(new Point(13, 7));
        obstacle2.add(new Point(11, 7));
        Obstacle o2 = new Obstacle(obstacle2);

        ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
        obstacles.add(o1);
        obstacles.add(o2);

        ArrayList<Point> workspace = new ArrayList<Point>();
        workspace.add(new Point(0.0,0.0));
        workspace.add(new Point(0.0,5.0));
        workspace.add(new Point(10.0,5.0));
        workspace.add(new Point(10.0,10.0));
        workspace.add(new Point(15.0,10.0));
        workspace.add(new Point(15.0,0.0));
        Obstacle w = new Obstacle(workspace);

        Robot r1 = new Robot(2, 2);
        Robot r2 = new Robot(12, 8);

        plot.addLinePlot("Obstacle 1", o1.getX(), o1.getY());
        plot.addLinePlot("Obstacle 2", Color.BLUE, o2.getX(), o2.getY());
        plot.addLinePlot("Workspace", Color.DARK_GRAY, w.getX(), w.getY());
        int robotPlot = plot.addScatterPlot("Robot", Color.RED, r1.getX(), r1.getY());
        plot.addScatterPlot("Robot", Color.RED, r2.getX(), r2.getY());

        //plot.setBounds(-2, -2, 19, 14);
        //plot.setFixedBounds(new double[]{-2, -2}, new double[]{17, 12});

        Point p1 = new Point(1,-5);
        Point p2 = new Point(0,0);
        Point p3 = new Point(0,5);

        System.out.println(GeometryTools.calculateAngleBetweenVectors(p1, p2, p3));

        ArrayList<Edge> result = GeometryTools.findTangentsToObstacle(new Point(2, 2), o1);
        ArrayList<Edge> result3 = GeometryTools.findTangentsToObstacle(new Point(12, 8), o2);
        result = GeometryTools.filterEdges(result, w);
        result3 = GeometryTools.filterEdges(result3, w);

        ArrayList<Edge> result2 = GeometryTools.findTangentsBetweenObstacles(o1, o2);

        result2 = GeometryTools.filterEdges(result2, w);

        result2.addAll(o1.getEdges());
        result2.addAll(o2.getEdges());
        //ArrayList<Edge> result2 = o1.getEdges();

        for (Edge e : result)
        {
            plot.addLinePlot(e.p1.toString(), Color.RED,
                    new double[][]{{e.p1.getX(), e.p1.getY()}, {e.p2.getX(), e.p2.getY()}});
        }

        for (Edge e : result2)
        {
            plot.addLinePlot(e.p1.toString(), Color.RED,
                    new double[][]{{e.p1.getX(), e.p1.getY()}, {e.p2.getX(), e.p2.getY()}});
        }

        for (Edge e : result3)
        {
            plot.addLinePlot(e.p1.toString(), Color.RED,
                    new double[][]{{e.p1.getX(), e.p1.getY()}, {e.p2.getX(), e.p2.getY()}});
        }


        ArrayList<Edge> allEdges = new ArrayList<Edge>();

        allEdges.addAll(result);
        allEdges.addAll(result2);
        allEdges.addAll(result3);

        HashMap<String, Node> visGraph = GeometryTools.constructTree(allEdges, new Point(2, 2), new Point(12, 8));

        ArrayList<Node> path = GeometryTools.DFS(new Point(2, 2), new Point(12, 8), visGraph);

        double X[] = new double[path.size()];
        double Y[] = new double[path.size()];

        int count = 0;

        for (Node node : path)
        {
            X[count] = node.getX();
            Y[count++] = node.getY();
        }

        plot.addLinePlot("Path", Color.GREEN, X, Y);



        plot.setVisible(true);
        //plot.setSize(600, 600);

        JFrame jframe = new JFrame("Plot");
        jframe.setSize(900, 600);
        jframe.setContentPane(plot);
        //jframe.pack();
        jframe.setVisible(true);
        //jframe.addWindowListener(new Exi);

        */
    }
}
