package stages;

/**
 * User: Dev
 * Date: 5/16/13
 * Time: 5:21 PM
 */
public class OpenGLStage
{
    public static void main(String args[])
    {
        int o = 2;
        int g = "QBJ".hashCode() % 3000;
        int u = "ZOO".hashCode() % 3000;
        for (int x = 0; x <= u; x++)
            o = (o ^ x) % g;
        System.out.println(o);
    }
}
