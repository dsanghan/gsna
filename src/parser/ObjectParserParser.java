// $ANTLR 3.5 ObjectParser.g 2013-06-05 22:28:42

    package parser;
    import datatypes.object.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ObjectParserParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BASIS_MATRIX", "BEVEL_INTERPOLATION", 
		"COLOR_INTERPOLATION", "COMMENT", "CONNECT", "CURVE", "CURVE2D", "CURVE_APPROX", 
		"CURVE_SURF_TYPE", "DECIMAL", "DEGREE", "DIGIT", "DISSOLVE_INTERPOLATION", 
		"END", "EXPONENT", "FACE", "GEOMETRIC_VERTEX", "GROUP_NAME", "INNER_TRIMMING_HOLE", 
		"INTEGER", "LEVEL_OF_DETAIL", "LINE", "MATERIAL_LIBRARY", "MATERIAL_NAME", 
		"MERGING_GROUP", "NAME", "NEWLINE", "OBJECT_NAME", "OUTER_TRIMMING_HOLE", 
		"PARAM", "PARAMETER_SPACE_VERTEX", "POINT", "RAY_TRACING", "SHADOW_CASTING", 
		"SMOOTHING_GROUP", "SPECIAL_CURVE", "SPECIAL_POINT", "STEP_SIZE", "SURF", 
		"SURF_APPROX", "TEXTURE_VERTEX", "VERTEX_NORMAL", "WS", "'/'", "'off'"
	};
	public static final int EOF=-1;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int BASIS_MATRIX=4;
	public static final int BEVEL_INTERPOLATION=5;
	public static final int COLOR_INTERPOLATION=6;
	public static final int COMMENT=7;
	public static final int CONNECT=8;
	public static final int CURVE=9;
	public static final int CURVE2D=10;
	public static final int CURVE_APPROX=11;
	public static final int CURVE_SURF_TYPE=12;
	public static final int DECIMAL=13;
	public static final int DEGREE=14;
	public static final int DIGIT=15;
	public static final int DISSOLVE_INTERPOLATION=16;
	public static final int END=17;
	public static final int EXPONENT=18;
	public static final int FACE=19;
	public static final int GEOMETRIC_VERTEX=20;
	public static final int GROUP_NAME=21;
	public static final int INNER_TRIMMING_HOLE=22;
	public static final int INTEGER=23;
	public static final int LEVEL_OF_DETAIL=24;
	public static final int LINE=25;
	public static final int MATERIAL_LIBRARY=26;
	public static final int MATERIAL_NAME=27;
	public static final int MERGING_GROUP=28;
	public static final int NAME=29;
	public static final int NEWLINE=30;
	public static final int OBJECT_NAME=31;
	public static final int OUTER_TRIMMING_HOLE=32;
	public static final int PARAM=33;
	public static final int PARAMETER_SPACE_VERTEX=34;
	public static final int POINT=35;
	public static final int RAY_TRACING=36;
	public static final int SHADOW_CASTING=37;
	public static final int SMOOTHING_GROUP=38;
	public static final int SPECIAL_CURVE=39;
	public static final int SPECIAL_POINT=40;
	public static final int STEP_SIZE=41;
	public static final int SURF=42;
	public static final int SURF_APPROX=43;
	public static final int TEXTURE_VERTEX=44;
	public static final int VERTEX_NORMAL=45;
	public static final int WS=46;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public ObjectParserParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public ObjectParserParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return ObjectParserParser.tokenNames; }
	@Override public String getGrammarFileName() { return "ObjectParser.g"; }



	// $ANTLR start "start"
	// ObjectParser.g:18:1: start : ( line )+ EOF ;
	public final void start() throws RecognitionException {
		try {
			// ObjectParser.g:18:7: ( ( line )+ EOF )
			// ObjectParser.g:18:9: ( line )+ EOF
			{
			// ObjectParser.g:18:9: ( line )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==COMMENT||(LA1_0 >= FACE && LA1_0 <= GROUP_NAME)||(LA1_0 >= MATERIAL_LIBRARY && LA1_0 <= MATERIAL_NAME)||(LA1_0 >= NEWLINE && LA1_0 <= OBJECT_NAME)||LA1_0==SMOOTHING_GROUP||(LA1_0 >= TEXTURE_VERTEX && LA1_0 <= VERTEX_NORMAL)) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// ObjectParser.g:18:10: line
					{
					pushFollow(FOLLOW_line_in_start29);
					line();
					state._fsp--;

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			match(input,EOF,FOLLOW_EOF_in_start33); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "start"



	// $ANTLR start "line"
	// ObjectParser.g:21:1: line : ( comment NEWLINE |v= vertex NEWLINE |vt= textureVertex NEWLINE |vn= normalVertex NEWLINE |f= face NEWLINE |mlib= mtllib NEWLINE | object NEWLINE | use_material NEWLINE | group NEWLINE | NEWLINE );
	public final void line() throws RecognitionException {
		Vertex v =null;
		Vertex vt =null;
		Vertex vn =null;
		Face f =null;
		String mlib =null;

		try {
			// ObjectParser.g:21:6: ( comment NEWLINE |v= vertex NEWLINE |vt= textureVertex NEWLINE |vn= normalVertex NEWLINE |f= face NEWLINE |mlib= mtllib NEWLINE | object NEWLINE | use_material NEWLINE | group NEWLINE | NEWLINE )
			int alt2=10;
			switch ( input.LA(1) ) {
			case COMMENT:
				{
				alt2=1;
				}
				break;
			case GEOMETRIC_VERTEX:
				{
				alt2=2;
				}
				break;
			case TEXTURE_VERTEX:
				{
				alt2=3;
				}
				break;
			case VERTEX_NORMAL:
				{
				alt2=4;
				}
				break;
			case FACE:
				{
				alt2=5;
				}
				break;
			case MATERIAL_LIBRARY:
				{
				alt2=6;
				}
				break;
			case OBJECT_NAME:
				{
				alt2=7;
				}
				break;
			case MATERIAL_NAME:
				{
				alt2=8;
				}
				break;
			case GROUP_NAME:
			case SMOOTHING_GROUP:
				{
				alt2=9;
				}
				break;
			case NEWLINE:
				{
				alt2=10;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// ObjectParser.g:21:10: comment NEWLINE
					{
					pushFollow(FOLLOW_comment_in_line46);
					comment();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line48); 
					}
					break;
				case 2 :
					// ObjectParser.g:22:15: v= vertex NEWLINE
					{
					pushFollow(FOLLOW_vertex_in_line66);
					v=vertex();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line68); 
					Obj.getObj(Obj.activeName).addV(v);
					}
					break;
				case 3 :
					// ObjectParser.g:23:15: vt= textureVertex NEWLINE
					{
					pushFollow(FOLLOW_textureVertex_in_line100);
					vt=textureVertex();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line102); 
					Obj.getObj(Obj.activeName).addVT(vt);
					}
					break;
				case 4 :
					// ObjectParser.g:24:15: vn= normalVertex NEWLINE
					{
					pushFollow(FOLLOW_normalVertex_in_line126);
					vn=normalVertex();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line128); 
					Obj.getObj(Obj.activeName).addVN(vn);
					}
					break;
				case 5 :
					// ObjectParser.g:25:15: f= face NEWLINE
					{
					pushFollow(FOLLOW_face_in_line153);
					f=face();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line155); 
					Obj.getObj(Obj.activeName).addFace(f);
					}
					break;
				case 6 :
					// ObjectParser.g:26:15: mlib= mtllib NEWLINE
					{
					pushFollow(FOLLOW_mtllib_in_line189);
					mlib=mtllib();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line191); 
					Material.parseMTLFile(mlib);
					}
					break;
				case 7 :
					// ObjectParser.g:27:15: object NEWLINE
					{
					pushFollow(FOLLOW_object_in_line218);
					object();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line220); 
					}
					break;
				case 8 :
					// ObjectParser.g:28:15: use_material NEWLINE
					{
					pushFollow(FOLLOW_use_material_in_line236);
					use_material();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line238); 
					}
					break;
				case 9 :
					// ObjectParser.g:29:15: group NEWLINE
					{
					pushFollow(FOLLOW_group_in_line254);
					group();
					state._fsp--;

					match(input,NEWLINE,FOLLOW_NEWLINE_in_line256); 
					}
					break;
				case 10 :
					// ObjectParser.g:30:15: NEWLINE
					{
					match(input,NEWLINE,FOLLOW_NEWLINE_in_line272); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "line"



	// $ANTLR start "comment"
	// ObjectParser.g:33:1: comment : COMMENT ;
	public final void comment() throws RecognitionException {
		try {
			// ObjectParser.g:33:9: ( COMMENT )
			// ObjectParser.g:33:11: COMMENT
			{
			match(input,COMMENT,FOLLOW_COMMENT_in_comment282); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "comment"



	// $ANTLR start "face"
	// ObjectParser.g:36:1: face returns [Face face] : FACE (x= faceVT |y= faceVTN |z= faceVN )+ ;
	public final Face face() throws RecognitionException {
		Face face = null;


		ParserRuleReturnScope x =null;
		ParserRuleReturnScope y =null;
		ParserRuleReturnScope z =null;

		try {
			// ObjectParser.g:36:25: ( FACE (x= faceVT |y= faceVTN |z= faceVN )+ )
			// ObjectParser.g:36:29: FACE (x= faceVT |y= faceVTN |z= faceVN )+
			{

			                                face = new Face();
			                            
			match(input,FACE,FOLLOW_FACE_in_face325); 
			// ObjectParser.g:39:31: (x= faceVT |y= faceVTN |z= faceVN )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=4;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==DECIMAL) ) {
					int LA3_2 = input.LA(2);
					if ( (LA3_2==47) ) {
						int LA3_3 = input.LA(3);
						if ( (LA3_3==DECIMAL) ) {
							int LA3_4 = input.LA(4);
							if ( (LA3_4==47) ) {
								alt3=2;
							}
							else if ( (LA3_4==DECIMAL||LA3_4==NEWLINE) ) {
								alt3=1;
							}

						}
						else if ( (LA3_3==47) ) {
							alt3=3;
						}

					}

				}

				switch (alt3) {
				case 1 :
					// ObjectParser.g:39:32: x= faceVT
					{
					pushFollow(FOLLOW_faceVT_in_face330);
					x=faceVT();
					state._fsp--;

					face.addV((x!=null?((ObjectParserParser.faceVT_return)x).v:null)); face.addVT((x!=null?((ObjectParserParser.faceVT_return)x).vt:null));
					}
					break;
				case 2 :
					// ObjectParser.g:40:32: y= faceVTN
					{
					pushFollow(FOLLOW_faceVTN_in_face367);
					y=faceVTN();
					state._fsp--;

					face.addV((y!=null?((ObjectParserParser.faceVTN_return)y).v:null)); face.addVT((y!=null?((ObjectParserParser.faceVTN_return)y).vt:null)); face.addVN((y!=null?((ObjectParserParser.faceVTN_return)y).vn:null));
					}
					break;
				case 3 :
					// ObjectParser.g:41:32: z= faceVN
					{
					pushFollow(FOLLOW_faceVN_in_face404);
					z=faceVN();
					state._fsp--;

					face.addV((z!=null?((ObjectParserParser.faceVN_return)z).v:null)); face.addVT((z!=null?((ObjectParserParser.faceVN_return)z).vn:null));
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return face;
	}
	// $ANTLR end "face"


	public static class faceVT_return extends ParserRuleReturnScope {
		public Vertex v;
		public Vertex vt;
	};


	// $ANTLR start "faceVT"
	// ObjectParser.g:44:1: faceVT returns [Vertex v, Vertex vt] : vIndex= DECIMAL '/' vtIndex= DECIMAL ;
	public final ObjectParserParser.faceVT_return faceVT() throws RecognitionException {
		ObjectParserParser.faceVT_return retval = new ObjectParserParser.faceVT_return();
		retval.start = input.LT(1);

		Token vIndex=null;
		Token vtIndex=null;

		try {
			// ObjectParser.g:44:37: (vIndex= DECIMAL '/' vtIndex= DECIMAL )
			// ObjectParser.g:44:39: vIndex= DECIMAL '/' vtIndex= DECIMAL
			{
			vIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVT423); 
			match(input,47,FOLLOW_47_in_faceVT425); 
			vtIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVT429); 

			            retval.v = Obj.getObj(Obj.activeName).getV(Integer.parseInt(vIndex.getText()));
			            retval.vt = Obj.getObj(Obj.activeName).getVT(Integer.parseInt(vtIndex.getText()));
			         
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "faceVT"


	public static class faceVTN_return extends ParserRuleReturnScope {
		public Vertex v;
		public Vertex vt;
		public Vertex vn;
	};


	// $ANTLR start "faceVTN"
	// ObjectParser.g:51:1: faceVTN returns [Vertex v, Vertex vt, Vertex vn] : vIndex= DECIMAL '/' vtIndex= DECIMAL '/' vnIndex= DECIMAL ;
	public final ObjectParserParser.faceVTN_return faceVTN() throws RecognitionException {
		ObjectParserParser.faceVTN_return retval = new ObjectParserParser.faceVTN_return();
		retval.start = input.LT(1);

		Token vIndex=null;
		Token vtIndex=null;
		Token vnIndex=null;

		try {
			// ObjectParser.g:51:49: (vIndex= DECIMAL '/' vtIndex= DECIMAL '/' vnIndex= DECIMAL )
			// ObjectParser.g:51:51: vIndex= DECIMAL '/' vtIndex= DECIMAL '/' vnIndex= DECIMAL
			{
			vIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVTN458); 
			match(input,47,FOLLOW_47_in_faceVTN460); 
			vtIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVTN464); 
			match(input,47,FOLLOW_47_in_faceVTN466); 
			vnIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVTN470); 

			            retval.v = Obj.getObj(Obj.activeName).getV(Integer.parseInt(vIndex.getText()));
			            retval.vt = Obj.getObj(Obj.activeName).getVT(Integer.parseInt(vtIndex.getText()));
			            retval.vn = Obj.getObj(Obj.activeName).getVN(Integer.parseInt(vnIndex.getText()));
			        
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "faceVTN"


	public static class faceVN_return extends ParserRuleReturnScope {
		public Vertex v;
		public Vertex vn;
	};


	// $ANTLR start "faceVN"
	// ObjectParser.g:59:1: faceVN returns [Vertex v, Vertex vn] : vIndex= DECIMAL '/' '/' vnIndex= DECIMAL ;
	public final ObjectParserParser.faceVN_return faceVN() throws RecognitionException {
		ObjectParserParser.faceVN_return retval = new ObjectParserParser.faceVN_return();
		retval.start = input.LT(1);

		Token vIndex=null;
		Token vnIndex=null;

		try {
			// ObjectParser.g:59:37: (vIndex= DECIMAL '/' '/' vnIndex= DECIMAL )
			// ObjectParser.g:59:39: vIndex= DECIMAL '/' '/' vnIndex= DECIMAL
			{
			vIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVN498); 
			match(input,47,FOLLOW_47_in_faceVN500); 
			match(input,47,FOLLOW_47_in_faceVN501); 
			vnIndex=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_faceVN505); 

			            retval.v = Obj.getObj(Obj.activeName).getV(Integer.parseInt(vIndex.getText()));
			            retval.vn = Obj.getObj(Obj.activeName).getVN(Integer.parseInt(vnIndex.getText()));
			        
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "faceVN"



	// $ANTLR start "vertex"
	// ObjectParser.g:65:1: vertex returns [Vertex v] : GEOMETRIC_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL ;
	public final Vertex vertex() throws RecognitionException {
		Vertex v = null;


		Token x=null;
		Token y=null;
		Token z=null;

		try {
			// ObjectParser.g:65:26: ( GEOMETRIC_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL )
			// ObjectParser.g:65:28: GEOMETRIC_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL
			{
			match(input,GEOMETRIC_VERTEX,FOLLOW_GEOMETRIC_VERTEX_in_vertex527); 
			x=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_vertex531); 
			y=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_vertex535); 
			z=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_vertex539); 

			                v = new Vertex(Double.parseDouble(x.getText()),
			                                Double.parseDouble(y.getText()),
			                                Double.parseDouble(z.getText()));
			            
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return v;
	}
	// $ANTLR end "vertex"



	// $ANTLR start "textureVertex"
	// ObjectParser.g:73:1: textureVertex returns [Vertex v] : ( TEXTURE_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL | TEXTURE_VERTEX x= DECIMAL y= DECIMAL );
	public final Vertex textureVertex() throws RecognitionException {
		Vertex v = null;


		Token x=null;
		Token y=null;
		Token z=null;

		try {
			// ObjectParser.g:73:33: ( TEXTURE_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL | TEXTURE_VERTEX x= DECIMAL y= DECIMAL )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==TEXTURE_VERTEX) ) {
				int LA4_1 = input.LA(2);
				if ( (LA4_1==DECIMAL) ) {
					int LA4_2 = input.LA(3);
					if ( (LA4_2==DECIMAL) ) {
						int LA4_3 = input.LA(4);
						if ( (LA4_3==DECIMAL) ) {
							alt4=1;
						}
						else if ( (LA4_3==NEWLINE) ) {
							alt4=2;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 4, 3, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 4, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 4, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// ObjectParser.g:73:35: TEXTURE_VERTEX x= DECIMAL y= DECIMAL z= DECIMAL
					{
					match(input,TEXTURE_VERTEX,FOLLOW_TEXTURE_VERTEX_in_textureVertex566); 
					x=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_textureVertex570); 
					y=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_textureVertex574); 
					z=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_textureVertex578); 

					                                                    v = new Vertex(Double.parseDouble(x.getText()),
					                                                                    Double.parseDouble(y.getText()),
					                                                                    Double.parseDouble(z.getText()));
					                                    
					}
					break;
				case 2 :
					// ObjectParser.g:79:35: TEXTURE_VERTEX x= DECIMAL y= DECIMAL
					{
					match(input,TEXTURE_VERTEX,FOLLOW_TEXTURE_VERTEX_in_textureVertex652); 
					x=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_textureVertex656); 
					y=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_textureVertex660); 

					                                                    v = new Vertex(Double.parseDouble(x.getText()),
					                                                                    Double.parseDouble(y.getText()),
					                                                                    0);
					                                    
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return v;
	}
	// $ANTLR end "textureVertex"



	// $ANTLR start "normalVertex"
	// ObjectParser.g:87:1: normalVertex returns [Vertex v] : VERTEX_NORMAL x= DECIMAL y= DECIMAL z= DECIMAL ;
	public final Vertex normalVertex() throws RecognitionException {
		Vertex v = null;


		Token x=null;
		Token y=null;
		Token z=null;

		try {
			// ObjectParser.g:87:32: ( VERTEX_NORMAL x= DECIMAL y= DECIMAL z= DECIMAL )
			// ObjectParser.g:87:34: VERTEX_NORMAL x= DECIMAL y= DECIMAL z= DECIMAL
			{
			match(input,VERTEX_NORMAL,FOLLOW_VERTEX_NORMAL_in_normalVertex714); 
			x=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_normalVertex718); 
			y=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_normalVertex722); 
			z=(Token)match(input,DECIMAL,FOLLOW_DECIMAL_in_normalVertex726); 

			                            v = new Vertex(Double.parseDouble(x.getText()),
			                                            Double.parseDouble(y.getText()),
			                                            Double.parseDouble(z.getText()));
			            
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return v;
	}
	// $ANTLR end "normalVertex"



	// $ANTLR start "mtllib"
	// ObjectParser.g:95:1: mtllib returns [String filename] : MATERIAL_LIBRARY a= NAME ;
	public final String mtllib() throws RecognitionException {
		String filename = null;


		Token a=null;

		try {
			// ObjectParser.g:95:33: ( MATERIAL_LIBRARY a= NAME )
			// ObjectParser.g:95:35: MATERIAL_LIBRARY a= NAME
			{
			match(input,MATERIAL_LIBRARY,FOLLOW_MATERIAL_LIBRARY_in_mtllib757); 
			a=(Token)match(input,NAME,FOLLOW_NAME_in_mtllib761); 

						filename = a.getText();
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return filename;
	}
	// $ANTLR end "mtllib"



	// $ANTLR start "object"
	// ObjectParser.g:101:1: object : OBJECT_NAME a= NAME ;
	public final void object() throws RecognitionException {
		Token a=null;

		try {
			// ObjectParser.g:101:8: ( OBJECT_NAME a= NAME )
			// ObjectParser.g:101:10: OBJECT_NAME a= NAME
			{
			match(input,OBJECT_NAME,FOLLOW_OBJECT_NAME_in_object776); 
			a=(Token)match(input,NAME,FOLLOW_NAME_in_object780); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "object"



	// $ANTLR start "use_material"
	// ObjectParser.g:104:1: use_material : MATERIAL_NAME a= NAME ;
	public final void use_material() throws RecognitionException {
		Token a=null;

		try {
			// ObjectParser.g:104:17: ( MATERIAL_NAME a= NAME )
			// ObjectParser.g:104:19: MATERIAL_NAME a= NAME
			{
			match(input,MATERIAL_NAME,FOLLOW_MATERIAL_NAME_in_use_material794); 
			a=(Token)match(input,NAME,FOLLOW_NAME_in_use_material798); 

			        Face.material_name = a.getText();
			    
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "use_material"



	// $ANTLR start "group"
	// ObjectParser.g:110:1: group : ( SMOOTHING_GROUP ( DECIMAL | 'off' ) | GROUP_NAME a= NAME | GROUP_NAME );
	public final void group() throws RecognitionException {
		Token a=null;

		try {
			// ObjectParser.g:110:7: ( SMOOTHING_GROUP ( DECIMAL | 'off' ) | GROUP_NAME a= NAME | GROUP_NAME )
			int alt5=3;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==SMOOTHING_GROUP) ) {
				alt5=1;
			}
			else if ( (LA5_0==GROUP_NAME) ) {
				int LA5_2 = input.LA(2);
				if ( (LA5_2==NAME) ) {
					alt5=2;
				}
				else if ( (LA5_2==NEWLINE) ) {
					alt5=3;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 5, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// ObjectParser.g:110:9: SMOOTHING_GROUP ( DECIMAL | 'off' )
					{
					match(input,SMOOTHING_GROUP,FOLLOW_SMOOTHING_GROUP_in_group814); 
					if ( input.LA(1)==DECIMAL||input.LA(1)==48 ) {
						input.consume();
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;
				case 2 :
					// ObjectParser.g:111:7: GROUP_NAME a= NAME
					{
					match(input,GROUP_NAME,FOLLOW_GROUP_NAME_in_group830); 
					a=(Token)match(input,NAME,FOLLOW_NAME_in_group834); 
					Obj.getObj(Obj.activeName).activeGroup = a.getText();
					}
					break;
				case 3 :
					// ObjectParser.g:112:7: GROUP_NAME
					{
					match(input,GROUP_NAME,FOLLOW_GROUP_NAME_in_group844); 
					Obj.getObj(Obj.activeName).activeGroup = "Default";
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "group"

	// Delegated rules



	public static final BitSet FOLLOW_line_in_start29 = new BitSet(new long[]{0x00003040CC380080L});
	public static final BitSet FOLLOW_EOF_in_start33 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_comment_in_line46 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line48 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_vertex_in_line66 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line68 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_textureVertex_in_line100 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line102 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalVertex_in_line126 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line128 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_face_in_line153 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line155 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_mtllib_in_line189 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line191 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_object_in_line218 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line220 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_use_material_in_line236 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line238 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_group_in_line254 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_NEWLINE_in_line256 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEWLINE_in_line272 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COMMENT_in_comment282 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FACE_in_face325 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_faceVT_in_face330 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_faceVTN_in_face367 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_faceVN_in_face404 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVT423 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_47_in_faceVT425 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVT429 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVTN458 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_47_in_faceVTN460 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVTN464 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_47_in_faceVTN466 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVTN470 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVN498 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_47_in_faceVN500 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_47_in_faceVN501 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_faceVN505 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GEOMETRIC_VERTEX_in_vertex527 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_vertex531 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_vertex535 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_vertex539 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TEXTURE_VERTEX_in_textureVertex566 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_textureVertex570 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_textureVertex574 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_textureVertex578 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TEXTURE_VERTEX_in_textureVertex652 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_textureVertex656 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_textureVertex660 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VERTEX_NORMAL_in_normalVertex714 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_normalVertex718 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_normalVertex722 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_DECIMAL_in_normalVertex726 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MATERIAL_LIBRARY_in_mtllib757 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_NAME_in_mtllib761 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OBJECT_NAME_in_object776 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_NAME_in_object780 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MATERIAL_NAME_in_use_material794 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_NAME_in_use_material798 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SMOOTHING_GROUP_in_group814 = new BitSet(new long[]{0x0001000000002000L});
	public static final BitSet FOLLOW_set_in_group816 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GROUP_NAME_in_group830 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_NAME_in_group834 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GROUP_NAME_in_group844 = new BitSet(new long[]{0x0000000000000002L});
}
