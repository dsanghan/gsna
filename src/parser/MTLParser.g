grammar MTLParser;

@header
{
    package parser;
    import datatypes.object.*;
}

@lexer::header
{
    package parser;
    import datatypes.object.Obj;
}

start : (line)+ EOF;

line : comment NEWLINE
     | new_mtl=new_material NEWLINE     {Material.activeMaterial = Material.getMaterial($new_mtl.name);}
     | od=optical_density NEWLINE       {Material.activeMaterial.optical_density = $od.val;}
     | se=specular_exponent NEWLINE     {Material.activeMaterial.specular_exponent = $se.val;}
     | d=dissolve NEWLINE               {Material.activeMaterial.dissolve = $d.val;}
     | t=tr NEWLINE                     {Material.activeMaterial.tr = $t.val;}
     | tf=transmission_filter NEWLINE   {Material.activeMaterial.transmission_filter = $tf.val;}
     | i=illumination NEWLINE           {Material.activeMaterial.illumination = $i.val;}
     | ac=ambient_color NEWLINE         {Material.activeMaterial.ambient_color = $ac.val;}
     | dc=diffuse_color NEWLINE         {Material.activeMaterial.diffuse_color = $dc.val;}
     | ec=emission_color NEWLINE        {Material.activeMaterial.emission_color = $ec.val;}
     | sc=specular_color NEWLINE        {Material.activeMaterial.specular_color = $sc.val;}
     | ap=ambient_map_path NEWLINE      {Material.activeMaterial.ambient_path = $ap.val;}
     | dp=diffuse_map_path NEWLINE      {Material.activeMaterial.diffuse_path = $dp.val;}
     | sp=specular_map_path NEWLINE     {Material.activeMaterial.specular_path = $sp.val;}
     | ep=emission_map_path NEWLINE     {Material.activeMaterial.emission_path = $ep.val;}
     | bp=bump_map_path NEWLINE         {Material.activeMaterial.bump_path = $bp.val;}
     | NEWLINE;

comment : COMMENT
;

new_material returns [String name]: 'newmtl' mat=NAME {$name = $mat.getText();}
;

optical_density returns [float val]: 'Ns' x=number {$val = $x.val;}
;

specular_exponent returns [float val]: 'Ni' x=number {$val = $x.val;}
;

dissolve returns [float val]: 'd' x=number  {$val = $x.val;}
;

tr returns [float val]: 'Tr' x=number {$val = $x.val;}
;

transmission_filter returns [float val[\]]: 'Tf' x=number y=number z=number {$val = new float[]{$x.val, $y.val, $z.val};}
;

illumination returns [float val]: 'illum' x=number {$val = $x.val;}
;

ambient_color returns [float val[\]]: 'Ka' x=number y=number z=number {$val = new float[]{$x.val, $y.val, $z.val, 1.0f};}
;

diffuse_color returns [float val[\]]: 'Kd' x=number y=number z=number {$val = new float[]{$x.val, $y.val, $z.val, 1.0f};}
;

specular_color returns [float val[\]]: 'Ks' x=number y=number z=number {$val = new float[]{$x.val, $y.val, $z.val, 1.0f};}
;

emission_color returns [float val[\]]: 'Ke' x=number y=number z=number {$val = new float[]{$x.val, $y.val, $z.val, 0.0f};}
;

ambient_map_path returns [String val]: 'map_Ka' x=NAME {$val = $x.getText();}
;

diffuse_map_path returns [String val]: 'map_Kd' x=NAME {$val = $x.getText();}
;

specular_map_path returns [String val]: 'map_Ks' x=NAME {$val = $x.getText();}
;

emission_map_path returns [String val]: 'map_Ke' x=NAME {$val = $x.getText();}
;

bump_map_path returns [String val]: ('map_bump' | 'bump' | 'map_Bump') x=NAME {$val = $x.getText();}
;

number returns [float val]:  x=DECIMAL {$val = Float.parseFloat($x.getText());}
;

fragment
DIGIT: '0'..'9';

NEWLINE : '\r'? '\n';

fragment INTEGER	:	'-'? (DIGIT)+
;

DECIMAL : INTEGER ('.' DIGIT*)?
;

COMMENT	: '#' ~('\n'|'\r')* {skip();}
	;

NAME	:	( 'A'..'Z' | 'a'..'z' | '0'..'9' | '-' | '_' | '~'| '(' | ')' | '\\' | '.')+
	;

WS: (' ' | '\t')+ {skip();}
;