// $ANTLR 3.5 ObjectParser.g 2013-06-05 22:28:43

    package parser;
    import datatypes.object.Obj;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ObjectParserLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int BASIS_MATRIX=4;
	public static final int BEVEL_INTERPOLATION=5;
	public static final int COLOR_INTERPOLATION=6;
	public static final int COMMENT=7;
	public static final int CONNECT=8;
	public static final int CURVE=9;
	public static final int CURVE2D=10;
	public static final int CURVE_APPROX=11;
	public static final int CURVE_SURF_TYPE=12;
	public static final int DECIMAL=13;
	public static final int DEGREE=14;
	public static final int DIGIT=15;
	public static final int DISSOLVE_INTERPOLATION=16;
	public static final int END=17;
	public static final int EXPONENT=18;
	public static final int FACE=19;
	public static final int GEOMETRIC_VERTEX=20;
	public static final int GROUP_NAME=21;
	public static final int INNER_TRIMMING_HOLE=22;
	public static final int INTEGER=23;
	public static final int LEVEL_OF_DETAIL=24;
	public static final int LINE=25;
	public static final int MATERIAL_LIBRARY=26;
	public static final int MATERIAL_NAME=27;
	public static final int MERGING_GROUP=28;
	public static final int NAME=29;
	public static final int NEWLINE=30;
	public static final int OBJECT_NAME=31;
	public static final int OUTER_TRIMMING_HOLE=32;
	public static final int PARAM=33;
	public static final int PARAMETER_SPACE_VERTEX=34;
	public static final int POINT=35;
	public static final int RAY_TRACING=36;
	public static final int SHADOW_CASTING=37;
	public static final int SMOOTHING_GROUP=38;
	public static final int SPECIAL_CURVE=39;
	public static final int SPECIAL_POINT=40;
	public static final int STEP_SIZE=41;
	public static final int SURF=42;
	public static final int SURF_APPROX=43;
	public static final int TEXTURE_VERTEX=44;
	public static final int VERTEX_NORMAL=45;
	public static final int WS=46;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ObjectParserLexer() {} 
	public ObjectParserLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ObjectParserLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "ObjectParser.g"; }

	// $ANTLR start "T__47"
	public final void mT__47() throws RecognitionException {
		try {
			int _type = T__47;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:7:7: ( '/' )
			// ObjectParser.g:7:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__47"

	// $ANTLR start "T__48"
	public final void mT__48() throws RecognitionException {
		try {
			int _type = T__48;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:8:7: ( 'off' )
			// ObjectParser.g:8:9: 'off'
			{
			match("off"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__48"

	// $ANTLR start "GEOMETRIC_VERTEX"
	public final void mGEOMETRIC_VERTEX() throws RecognitionException {
		try {
			int _type = GEOMETRIC_VERTEX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:117:2: ( 'v' )
			// ObjectParser.g:117:4: 'v'
			{
			match('v'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GEOMETRIC_VERTEX"

	// $ANTLR start "TEXTURE_VERTEX"
	public final void mTEXTURE_VERTEX() throws RecognitionException {
		try {
			int _type = TEXTURE_VERTEX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:121:2: ( 'vt' )
			// ObjectParser.g:121:4: 'vt'
			{
			match("vt"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TEXTURE_VERTEX"

	// $ANTLR start "VERTEX_NORMAL"
	public final void mVERTEX_NORMAL() throws RecognitionException {
		try {
			int _type = VERTEX_NORMAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:125:2: ( 'vn' )
			// ObjectParser.g:125:4: 'vn'
			{
			match("vn"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VERTEX_NORMAL"

	// $ANTLR start "PARAMETER_SPACE_VERTEX"
	public final void mPARAMETER_SPACE_VERTEX() throws RecognitionException {
		try {
			int _type = PARAMETER_SPACE_VERTEX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:129:2: ( 'vp' )
			// ObjectParser.g:129:4: 'vp'
			{
			match("vp"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PARAMETER_SPACE_VERTEX"

	// $ANTLR start "DEGREE"
	public final void mDEGREE() throws RecognitionException {
		try {
			int _type = DEGREE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:133:8: ( 'deg' )
			// ObjectParser.g:133:10: 'deg'
			{
			match("deg"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DEGREE"

	// $ANTLR start "BASIS_MATRIX"
	public final void mBASIS_MATRIX() throws RecognitionException {
		try {
			int _type = BASIS_MATRIX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:136:2: ( 'bmat' )
			// ObjectParser.g:136:4: 'bmat'
			{
			match("bmat"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BASIS_MATRIX"

	// $ANTLR start "STEP_SIZE"
	public final void mSTEP_SIZE() throws RecognitionException {
		try {
			int _type = STEP_SIZE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:140:2: ( 'step' )
			// ObjectParser.g:140:4: 'step'
			{
			match("step"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STEP_SIZE"

	// $ANTLR start "CURVE_SURF_TYPE"
	public final void mCURVE_SURF_TYPE() throws RecognitionException {
		try {
			int _type = CURVE_SURF_TYPE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:144:2: ( 'cstype' )
			// ObjectParser.g:144:4: 'cstype'
			{
			match("cstype"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CURVE_SURF_TYPE"

	// $ANTLR start "POINT"
	public final void mPOINT() throws RecognitionException {
		try {
			int _type = POINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:148:7: ( 'p' )
			// ObjectParser.g:148:9: 'p'
			{
			match('p'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POINT"

	// $ANTLR start "LINE"
	public final void mLINE() throws RecognitionException {
		try {
			int _type = LINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:150:6: ( 'l' )
			// ObjectParser.g:150:8: 'l'
			{
			match('l'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE"

	// $ANTLR start "FACE"
	public final void mFACE() throws RecognitionException {
		try {
			int _type = FACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:152:6: ( 'f' )
			// ObjectParser.g:152:8: 'f'
			{
			match('f'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FACE"

	// $ANTLR start "CURVE"
	public final void mCURVE() throws RecognitionException {
		try {
			int _type = CURVE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:154:7: ( 'curv' )
			// ObjectParser.g:154:9: 'curv'
			{
			match("curv"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CURVE"

	// $ANTLR start "CURVE2D"
	public final void mCURVE2D() throws RecognitionException {
		try {
			int _type = CURVE2D;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:156:9: ( 'curv2' )
			// ObjectParser.g:156:11: 'curv2'
			{
			match("curv2"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CURVE2D"

	// $ANTLR start "SURF"
	public final void mSURF() throws RecognitionException {
		try {
			int _type = SURF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:158:6: ( 'surf' )
			// ObjectParser.g:158:8: 'surf'
			{
			match("surf"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SURF"

	// $ANTLR start "PARAM"
	public final void mPARAM() throws RecognitionException {
		try {
			int _type = PARAM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:161:7: ( 'parm' )
			// ObjectParser.g:161:9: 'parm'
			{
			match("parm"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PARAM"

	// $ANTLR start "OUTER_TRIMMING_HOLE"
	public final void mOUTER_TRIMMING_HOLE() throws RecognitionException {
		try {
			int _type = OUTER_TRIMMING_HOLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:164:2: ( 'trim' )
			// ObjectParser.g:164:4: 'trim'
			{
			match("trim"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OUTER_TRIMMING_HOLE"

	// $ANTLR start "INNER_TRIMMING_HOLE"
	public final void mINNER_TRIMMING_HOLE() throws RecognitionException {
		try {
			int _type = INNER_TRIMMING_HOLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:168:2: ( 'hole' )
			// ObjectParser.g:168:4: 'hole'
			{
			match("hole"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INNER_TRIMMING_HOLE"

	// $ANTLR start "SPECIAL_CURVE"
	public final void mSPECIAL_CURVE() throws RecognitionException {
		try {
			int _type = SPECIAL_CURVE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:172:2: ( 'scrv' )
			// ObjectParser.g:172:4: 'scrv'
			{
			match("scrv"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPECIAL_CURVE"

	// $ANTLR start "SPECIAL_POINT"
	public final void mSPECIAL_POINT() throws RecognitionException {
		try {
			int _type = SPECIAL_POINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:176:2: ( 'sp' )
			// ObjectParser.g:176:4: 'sp'
			{
			match("sp"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPECIAL_POINT"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:179:5: ( 'end' )
			// ObjectParser.g:179:7: 'end'
			{
			match("end"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "CONNECT"
	public final void mCONNECT() throws RecognitionException {
		try {
			int _type = CONNECT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:182:9: ( 'con' )
			// ObjectParser.g:182:11: 'con'
			{
			match("con"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONNECT"

	// $ANTLR start "GROUP_NAME"
	public final void mGROUP_NAME() throws RecognitionException {
		try {
			int _type = GROUP_NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:186:2: ( 'g' )
			// ObjectParser.g:186:4: 'g'
			{
			match('g'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GROUP_NAME"

	// $ANTLR start "SMOOTHING_GROUP"
	public final void mSMOOTHING_GROUP() throws RecognitionException {
		try {
			int _type = SMOOTHING_GROUP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:190:2: ( 's' )
			// ObjectParser.g:190:4: 's'
			{
			match('s'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SMOOTHING_GROUP"

	// $ANTLR start "MERGING_GROUP"
	public final void mMERGING_GROUP() throws RecognitionException {
		try {
			int _type = MERGING_GROUP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:194:2: ( 'mg' )
			// ObjectParser.g:194:4: 'mg'
			{
			match("mg"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MERGING_GROUP"

	// $ANTLR start "OBJECT_NAME"
	public final void mOBJECT_NAME() throws RecognitionException {
		try {
			int _type = OBJECT_NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:198:2: ( 'o' )
			// ObjectParser.g:198:4: 'o'
			{
			match('o'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OBJECT_NAME"

	// $ANTLR start "BEVEL_INTERPOLATION"
	public final void mBEVEL_INTERPOLATION() throws RecognitionException {
		try {
			int _type = BEVEL_INTERPOLATION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:203:2: ( 'bevel' )
			// ObjectParser.g:203:4: 'bevel'
			{
			match("bevel"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BEVEL_INTERPOLATION"

	// $ANTLR start "COLOR_INTERPOLATION"
	public final void mCOLOR_INTERPOLATION() throws RecognitionException {
		try {
			int _type = COLOR_INTERPOLATION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:207:2: ( 'c_interp' )
			// ObjectParser.g:207:4: 'c_interp'
			{
			match("c_interp"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLOR_INTERPOLATION"

	// $ANTLR start "DISSOLVE_INTERPOLATION"
	public final void mDISSOLVE_INTERPOLATION() throws RecognitionException {
		try {
			int _type = DISSOLVE_INTERPOLATION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:211:2: ( 'd_interp' )
			// ObjectParser.g:211:4: 'd_interp'
			{
			match("d_interp"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DISSOLVE_INTERPOLATION"

	// $ANTLR start "LEVEL_OF_DETAIL"
	public final void mLEVEL_OF_DETAIL() throws RecognitionException {
		try {
			int _type = LEVEL_OF_DETAIL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:215:2: ( 'lod' )
			// ObjectParser.g:215:4: 'lod'
			{
			match("lod"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEVEL_OF_DETAIL"

	// $ANTLR start "MATERIAL_NAME"
	public final void mMATERIAL_NAME() throws RecognitionException {
		try {
			int _type = MATERIAL_NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:219:2: ( 'usemtl' )
			// ObjectParser.g:219:4: 'usemtl'
			{
			match("usemtl"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MATERIAL_NAME"

	// $ANTLR start "MATERIAL_LIBRARY"
	public final void mMATERIAL_LIBRARY() throws RecognitionException {
		try {
			int _type = MATERIAL_LIBRARY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:223:2: ( 'mtllib' )
			// ObjectParser.g:223:4: 'mtllib'
			{
			match("mtllib"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MATERIAL_LIBRARY"

	// $ANTLR start "SHADOW_CASTING"
	public final void mSHADOW_CASTING() throws RecognitionException {
		try {
			int _type = SHADOW_CASTING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:227:2: ( 'shadow_obj' )
			// ObjectParser.g:227:4: 'shadow_obj'
			{
			match("shadow_obj"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SHADOW_CASTING"

	// $ANTLR start "RAY_TRACING"
	public final void mRAY_TRACING() throws RecognitionException {
		try {
			int _type = RAY_TRACING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:231:2: ( 'trace_obj' )
			// ObjectParser.g:231:4: 'trace_obj'
			{
			match("trace_obj"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RAY_TRACING"

	// $ANTLR start "CURVE_APPROX"
	public final void mCURVE_APPROX() throws RecognitionException {
		try {
			int _type = CURVE_APPROX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:235:2: ( 'ctech' )
			// ObjectParser.g:235:4: 'ctech'
			{
			match("ctech"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CURVE_APPROX"

	// $ANTLR start "SURF_APPROX"
	public final void mSURF_APPROX() throws RecognitionException {
		try {
			int _type = SURF_APPROX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:239:2: ( 'stech' )
			// ObjectParser.g:239:4: 'stech'
			{
			match("stech"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SURF_APPROX"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// ObjectParser.g:244:6: ( '0' .. '9' )
			// ObjectParser.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			int _type = NEWLINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:245:9: ( ( '\\r' )? '\\n' )
			// ObjectParser.g:245:11: ( '\\r' )? '\\n'
			{
			// ObjectParser.g:245:11: ( '\\r' )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='\r') ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// ObjectParser.g:245:11: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			// ObjectParser.g:247:18: ( ( '-' )? ( DIGIT )+ )
			// ObjectParser.g:247:20: ( '-' )? ( DIGIT )+
			{
			// ObjectParser.g:247:20: ( '-' )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='-') ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// ObjectParser.g:247:20: '-'
					{
					match('-'); 
					}
					break;

			}

			// ObjectParser.g:247:25: ( DIGIT )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// ObjectParser.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "EXPONENT"
	public final void mEXPONENT() throws RecognitionException {
		try {
			// ObjectParser.g:251:9: ( ( 'e' | 'E' ) ( '+' )? ( INTEGER ) )
			// ObjectParser.g:251:11: ( 'e' | 'E' ) ( '+' )? ( INTEGER )
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// ObjectParser.g:251:23: ( '+' )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0=='+') ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// ObjectParser.g:251:24: '+'
					{
					match('+'); 
					}
					break;

			}

			// ObjectParser.g:251:30: ( INTEGER )
			// ObjectParser.g:251:31: INTEGER
			{
			mINTEGER(); 

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPONENT"

	// $ANTLR start "DECIMAL"
	public final void mDECIMAL() throws RecognitionException {
		try {
			int _type = DECIMAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:252:9: ( INTEGER ( '.' ( DIGIT )* ( EXPONENT )? )? )
			// ObjectParser.g:252:11: INTEGER ( '.' ( DIGIT )* ( EXPONENT )? )?
			{
			mINTEGER(); 

			// ObjectParser.g:252:19: ( '.' ( DIGIT )* ( EXPONENT )? )?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='.') ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// ObjectParser.g:252:20: '.' ( DIGIT )* ( EXPONENT )?
					{
					match('.'); 
					// ObjectParser.g:252:24: ( DIGIT )*
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// ObjectParser.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop5;
						}
					}

					// ObjectParser.g:252:33: ( EXPONENT )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0=='E'||LA6_0=='e') ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// ObjectParser.g:252:34: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DECIMAL"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:255:9: ( '#' (~ ( '\\n' | '\\r' ) )* )
			// ObjectParser.g:255:11: '#' (~ ( '\\n' | '\\r' ) )*
			{
			match('#'); 
			// ObjectParser.g:255:15: (~ ( '\\n' | '\\r' ) )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\t')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\uFFFF')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// ObjectParser.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop8;
				}
			}

			skip();
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "NAME"
	public final void mNAME() throws RecognitionException {
		try {
			int _type = NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:258:6: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '.' | '-' | '_' | '~' | '(' | ')' )+ )
			// ObjectParser.g:258:8: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '.' | '-' | '_' | '~' | '(' | ')' )+
			{
			// ObjectParser.g:258:8: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '.' | '-' | '_' | '~' | '(' | ')' )+
			int cnt9=0;
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( ((LA9_0 >= '(' && LA9_0 <= ')')||(LA9_0 >= '-' && LA9_0 <= '.')||(LA9_0 >= '0' && LA9_0 <= '9')||(LA9_0 >= 'A' && LA9_0 <= 'Z')||LA9_0=='_'||(LA9_0 >= 'a' && LA9_0 <= 'z')||LA9_0=='~') ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// ObjectParser.g:
					{
					if ( (input.LA(1) >= '(' && input.LA(1) <= ')')||(input.LA(1) >= '-' && input.LA(1) <= '.')||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z')||input.LA(1)=='~' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt9 >= 1 ) break loop9;
					EarlyExitException eee = new EarlyExitException(9, input);
					throw eee;
				}
				cnt9++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NAME"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// ObjectParser.g:261:3: ( ( ' ' | '\\t' )+ )
			// ObjectParser.g:261:5: ( ' ' | '\\t' )+
			{
			// ObjectParser.g:261:5: ( ' ' | '\\t' )+
			int cnt10=0;
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0=='\t'||LA10_0==' ') ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// ObjectParser.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt10 >= 1 ) break loop10;
					EarlyExitException eee = new EarlyExitException(10, input);
					throw eee;
				}
				cnt10++;
			}

			skip();
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// ObjectParser.g:1:8: ( T__47 | T__48 | GEOMETRIC_VERTEX | TEXTURE_VERTEX | VERTEX_NORMAL | PARAMETER_SPACE_VERTEX | DEGREE | BASIS_MATRIX | STEP_SIZE | CURVE_SURF_TYPE | POINT | LINE | FACE | CURVE | CURVE2D | SURF | PARAM | OUTER_TRIMMING_HOLE | INNER_TRIMMING_HOLE | SPECIAL_CURVE | SPECIAL_POINT | END | CONNECT | GROUP_NAME | SMOOTHING_GROUP | MERGING_GROUP | OBJECT_NAME | BEVEL_INTERPOLATION | COLOR_INTERPOLATION | DISSOLVE_INTERPOLATION | LEVEL_OF_DETAIL | MATERIAL_NAME | MATERIAL_LIBRARY | SHADOW_CASTING | RAY_TRACING | CURVE_APPROX | SURF_APPROX | NEWLINE | DECIMAL | COMMENT | NAME | WS )
		int alt11=42;
		alt11 = dfa11.predict(input);
		switch (alt11) {
			case 1 :
				// ObjectParser.g:1:10: T__47
				{
				mT__47(); 

				}
				break;
			case 2 :
				// ObjectParser.g:1:16: T__48
				{
				mT__48(); 

				}
				break;
			case 3 :
				// ObjectParser.g:1:22: GEOMETRIC_VERTEX
				{
				mGEOMETRIC_VERTEX(); 

				}
				break;
			case 4 :
				// ObjectParser.g:1:39: TEXTURE_VERTEX
				{
				mTEXTURE_VERTEX(); 

				}
				break;
			case 5 :
				// ObjectParser.g:1:54: VERTEX_NORMAL
				{
				mVERTEX_NORMAL(); 

				}
				break;
			case 6 :
				// ObjectParser.g:1:68: PARAMETER_SPACE_VERTEX
				{
				mPARAMETER_SPACE_VERTEX(); 

				}
				break;
			case 7 :
				// ObjectParser.g:1:91: DEGREE
				{
				mDEGREE(); 

				}
				break;
			case 8 :
				// ObjectParser.g:1:98: BASIS_MATRIX
				{
				mBASIS_MATRIX(); 

				}
				break;
			case 9 :
				// ObjectParser.g:1:111: STEP_SIZE
				{
				mSTEP_SIZE(); 

				}
				break;
			case 10 :
				// ObjectParser.g:1:121: CURVE_SURF_TYPE
				{
				mCURVE_SURF_TYPE(); 

				}
				break;
			case 11 :
				// ObjectParser.g:1:137: POINT
				{
				mPOINT(); 

				}
				break;
			case 12 :
				// ObjectParser.g:1:143: LINE
				{
				mLINE(); 

				}
				break;
			case 13 :
				// ObjectParser.g:1:148: FACE
				{
				mFACE(); 

				}
				break;
			case 14 :
				// ObjectParser.g:1:153: CURVE
				{
				mCURVE(); 

				}
				break;
			case 15 :
				// ObjectParser.g:1:159: CURVE2D
				{
				mCURVE2D(); 

				}
				break;
			case 16 :
				// ObjectParser.g:1:167: SURF
				{
				mSURF(); 

				}
				break;
			case 17 :
				// ObjectParser.g:1:172: PARAM
				{
				mPARAM(); 

				}
				break;
			case 18 :
				// ObjectParser.g:1:178: OUTER_TRIMMING_HOLE
				{
				mOUTER_TRIMMING_HOLE(); 

				}
				break;
			case 19 :
				// ObjectParser.g:1:198: INNER_TRIMMING_HOLE
				{
				mINNER_TRIMMING_HOLE(); 

				}
				break;
			case 20 :
				// ObjectParser.g:1:218: SPECIAL_CURVE
				{
				mSPECIAL_CURVE(); 

				}
				break;
			case 21 :
				// ObjectParser.g:1:232: SPECIAL_POINT
				{
				mSPECIAL_POINT(); 

				}
				break;
			case 22 :
				// ObjectParser.g:1:246: END
				{
				mEND(); 

				}
				break;
			case 23 :
				// ObjectParser.g:1:250: CONNECT
				{
				mCONNECT(); 

				}
				break;
			case 24 :
				// ObjectParser.g:1:258: GROUP_NAME
				{
				mGROUP_NAME(); 

				}
				break;
			case 25 :
				// ObjectParser.g:1:269: SMOOTHING_GROUP
				{
				mSMOOTHING_GROUP(); 

				}
				break;
			case 26 :
				// ObjectParser.g:1:285: MERGING_GROUP
				{
				mMERGING_GROUP(); 

				}
				break;
			case 27 :
				// ObjectParser.g:1:299: OBJECT_NAME
				{
				mOBJECT_NAME(); 

				}
				break;
			case 28 :
				// ObjectParser.g:1:311: BEVEL_INTERPOLATION
				{
				mBEVEL_INTERPOLATION(); 

				}
				break;
			case 29 :
				// ObjectParser.g:1:331: COLOR_INTERPOLATION
				{
				mCOLOR_INTERPOLATION(); 

				}
				break;
			case 30 :
				// ObjectParser.g:1:351: DISSOLVE_INTERPOLATION
				{
				mDISSOLVE_INTERPOLATION(); 

				}
				break;
			case 31 :
				// ObjectParser.g:1:374: LEVEL_OF_DETAIL
				{
				mLEVEL_OF_DETAIL(); 

				}
				break;
			case 32 :
				// ObjectParser.g:1:390: MATERIAL_NAME
				{
				mMATERIAL_NAME(); 

				}
				break;
			case 33 :
				// ObjectParser.g:1:404: MATERIAL_LIBRARY
				{
				mMATERIAL_LIBRARY(); 

				}
				break;
			case 34 :
				// ObjectParser.g:1:421: SHADOW_CASTING
				{
				mSHADOW_CASTING(); 

				}
				break;
			case 35 :
				// ObjectParser.g:1:436: RAY_TRACING
				{
				mRAY_TRACING(); 

				}
				break;
			case 36 :
				// ObjectParser.g:1:448: CURVE_APPROX
				{
				mCURVE_APPROX(); 

				}
				break;
			case 37 :
				// ObjectParser.g:1:461: SURF_APPROX
				{
				mSURF_APPROX(); 

				}
				break;
			case 38 :
				// ObjectParser.g:1:473: NEWLINE
				{
				mNEWLINE(); 

				}
				break;
			case 39 :
				// ObjectParser.g:1:481: DECIMAL
				{
				mDECIMAL(); 

				}
				break;
			case 40 :
				// ObjectParser.g:1:489: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 41 :
				// ObjectParser.g:1:497: NAME
				{
				mNAME(); 

				}
				break;
			case 42 :
				// ObjectParser.g:1:502: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA11 dfa11 = new DFA11(this);
	static final String DFA11_eotS =
		"\2\uffff\1\30\1\34\2\25\1\46\1\25\1\55\1\57\1\60\3\25\1\64\2\25\1\uffff"+
		"\1\25\1\70\3\uffff\1\25\1\uffff\1\73\1\74\1\75\1\uffff\7\25\1\105\1\25"+
		"\1\uffff\6\25\1\uffff\1\25\2\uffff\3\25\1\uffff\1\122\2\25\1\uffff\1\70"+
		"\1\127\3\uffff\1\130\6\25\1\uffff\3\25\1\143\3\25\1\147\3\25\1\153\1\uffff"+
		"\2\25\1\70\1\25\2\uffff\1\25\1\161\1\25\1\163\1\25\1\165\1\166\2\25\1"+
		"\172\1\uffff\2\25\1\175\1\uffff\1\176\1\25\1\u0080\1\uffff\3\25\1\70\1"+
		"\25\1\uffff\1\u0084\1\uffff\1\u0085\2\uffff\2\25\1\u0088\1\uffff\1\25"+
		"\1\u008a\2\uffff\1\25\1\uffff\3\25\2\uffff\1\25\1\u0090\1\uffff\1\25\1"+
		"\uffff\1\25\1\u0093\1\u0094\2\25\1\uffff\2\25\2\uffff\1\u0099\1\25\1\u009b"+
		"\1\25\1\uffff\1\25\1\uffff\1\u009e\1\u009f\2\uffff";
	static final String DFA11_eofS =
		"\u00a0\uffff";
	static final String DFA11_minS =
		"\1\11\1\uffff\2\50\1\137\1\145\1\50\1\137\3\50\1\162\1\157\1\156\1\50"+
		"\1\147\1\163\1\uffff\1\60\1\50\3\uffff\1\146\1\uffff\3\50\1\uffff\1\147"+
		"\1\151\1\141\1\166\1\145\2\162\1\50\1\141\1\uffff\1\164\1\162\1\156\1"+
		"\151\1\145\1\162\1\uffff\1\144\2\uffff\1\141\1\154\1\144\1\uffff\1\50"+
		"\1\154\1\145\1\uffff\2\50\3\uffff\1\50\1\156\1\164\1\145\1\143\1\146\1"+
		"\166\1\uffff\1\144\1\171\1\166\1\50\1\156\1\143\1\155\1\50\1\155\1\143"+
		"\1\145\1\50\1\uffff\1\154\1\155\1\50\1\53\2\uffff\1\164\1\50\1\154\1\50"+
		"\1\150\2\50\1\157\1\160\1\50\1\uffff\1\164\1\150\1\50\1\uffff\1\50\1\145"+
		"\1\50\1\uffff\1\151\1\164\1\60\1\50\1\145\1\uffff\1\50\1\uffff\1\50\2"+
		"\uffff\1\167\1\145\1\50\1\uffff\1\145\1\50\2\uffff\1\137\1\uffff\1\142"+
		"\1\154\1\162\2\uffff\1\137\1\50\1\uffff\1\162\1\uffff\1\157\2\50\1\160"+
		"\1\157\1\uffff\1\160\1\142\2\uffff\1\50\1\142\1\50\1\152\1\uffff\1\152"+
		"\1\uffff\2\50\2\uffff";
	static final String DFA11_maxS =
		"\1\176\1\uffff\2\176\1\145\1\155\1\176\1\165\3\176\1\162\1\157\1\156\1"+
		"\176\1\164\1\163\1\uffff\1\71\1\176\3\uffff\1\146\1\uffff\3\176\1\uffff"+
		"\1\147\1\151\1\141\1\166\1\145\2\162\1\176\1\141\1\uffff\1\164\1\162\1"+
		"\156\1\151\1\145\1\162\1\uffff\1\144\2\uffff\1\151\1\154\1\144\1\uffff"+
		"\1\176\1\154\1\145\1\uffff\2\176\3\uffff\1\176\1\156\1\164\1\145\1\160"+
		"\1\146\1\166\1\uffff\1\144\1\171\1\166\1\176\1\156\1\143\1\155\1\176\1"+
		"\155\1\143\1\145\1\176\1\uffff\1\154\1\155\1\176\1\71\2\uffff\1\164\1"+
		"\176\1\154\1\176\1\150\2\176\1\157\1\160\1\176\1\uffff\1\164\1\150\1\176"+
		"\1\uffff\1\176\1\145\1\176\1\uffff\1\151\1\164\1\71\1\176\1\145\1\uffff"+
		"\1\176\1\uffff\1\176\2\uffff\1\167\1\145\1\176\1\uffff\1\145\1\176\2\uffff"+
		"\1\137\1\uffff\1\142\1\154\1\162\2\uffff\1\137\1\176\1\uffff\1\162\1\uffff"+
		"\1\157\2\176\1\160\1\157\1\uffff\1\160\1\142\2\uffff\1\176\1\142\1\176"+
		"\1\152\1\uffff\1\152\1\uffff\2\176\2\uffff";
	static final String DFA11_acceptS =
		"\1\uffff\1\1\17\uffff\1\46\2\uffff\1\50\1\51\1\52\1\uffff\1\33\3\uffff"+
		"\1\3\11\uffff\1\31\6\uffff\1\13\1\uffff\1\14\1\15\3\uffff\1\30\3\uffff"+
		"\1\47\2\uffff\1\4\1\5\1\6\7\uffff\1\25\14\uffff\1\32\4\uffff\1\2\1\7\12"+
		"\uffff\1\27\3\uffff\1\37\3\uffff\1\26\5\uffff\1\10\1\uffff\1\11\1\uffff"+
		"\1\20\1\24\3\uffff\1\16\2\uffff\1\21\1\22\1\uffff\1\23\3\uffff\1\34\1"+
		"\45\2\uffff\1\17\1\uffff\1\44\5\uffff\1\12\2\uffff\1\41\1\40\4\uffff\1"+
		"\36\1\uffff\1\35\2\uffff\1\43\1\42";
	static final String DFA11_specialS =
		"\u00a0\uffff}>";
	static final String[] DFA11_transitionS = {
			"\1\26\1\21\2\uffff\1\21\22\uffff\1\26\2\uffff\1\24\4\uffff\2\25\3\uffff"+
			"\1\22\1\25\1\1\12\23\7\uffff\32\25\4\uffff\1\25\1\uffff\1\25\1\5\1\7"+
			"\1\4\1\15\1\12\1\16\1\14\3\25\1\11\1\17\1\25\1\2\1\10\2\25\1\6\1\13\1"+
			"\20\1\3\4\25\3\uffff\1\25",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\5\25\1\27\24\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\15\25\1\32\1\25\1\33\3\25\1\31\6\25\3\uffff\1\25",
			"\1\36\5\uffff\1\35",
			"\1\40\7\uffff\1\37",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\2\25\1\43\4\25\1\45\7\25\1\44\3\25\1\41\1\42\5\25\3\uffff\1\25",
			"\1\52\17\uffff\1\51\3\uffff\1\47\1\53\1\50",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\1\54\31\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\16\25\1\56\13\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\61",
			"\1\62",
			"\1\63",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\65\14\uffff\1\66",
			"\1\67",
			"",
			"\12\23",
			"\2\25\3\uffff\1\25\1\71\1\uffff\12\23\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"",
			"",
			"\1\72",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\1\76",
			"\1\77",
			"\1\100",
			"\1\101",
			"\1\102",
			"\1\103",
			"\1\104",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\106",
			"",
			"\1\107",
			"\1\110",
			"\1\111",
			"\1\112",
			"\1\113",
			"\1\114",
			"",
			"\1\115",
			"",
			"",
			"\1\117\7\uffff\1\116",
			"\1\120",
			"\1\121",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\123",
			"\1\124",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\125\7\uffff\4\25\1\126\25\25\4\uffff\1"+
			"\25\1\uffff\4\25\1\126\25\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\131",
			"\1\132",
			"\1\133",
			"\1\135\14\uffff\1\134",
			"\1\136",
			"\1\137",
			"",
			"\1\140",
			"\1\141",
			"\1\142",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\144",
			"\1\145",
			"\1\146",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\150",
			"\1\151",
			"\1\152",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\1\154",
			"\1\155",
			"\2\25\3\uffff\2\25\1\uffff\12\125\7\uffff\4\25\1\126\25\25\4\uffff\1"+
			"\25\1\uffff\4\25\1\126\25\25\3\uffff\1\25",
			"\1\70\1\uffff\1\156\2\uffff\12\157",
			"",
			"",
			"\1\160",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\162",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\164",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\167",
			"\1\170",
			"\2\25\3\uffff\2\25\1\uffff\2\25\1\171\7\25\7\uffff\32\25\4\uffff\1\25"+
			"\1\uffff\32\25\3\uffff\1\25",
			"",
			"\1\173",
			"\1\174",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\177",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\1\u0081",
			"\1\u0082",
			"\12\157",
			"\2\25\3\uffff\2\25\1\uffff\12\157\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\u0083",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"",
			"\1\u0086",
			"\1\u0087",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\1\u0089",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"",
			"\1\u008b",
			"",
			"\1\u008c",
			"\1\u008d",
			"\1\u008e",
			"",
			"",
			"\1\u008f",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			"\1\u0091",
			"",
			"\1\u0092",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\u0095",
			"\1\u0096",
			"",
			"\1\u0097",
			"\1\u0098",
			"",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\u009a",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\1\u009c",
			"",
			"\1\u009d",
			"",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"\2\25\3\uffff\2\25\1\uffff\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff"+
			"\32\25\3\uffff\1\25",
			"",
			""
	};

	static final short[] DFA11_eot = DFA.unpackEncodedString(DFA11_eotS);
	static final short[] DFA11_eof = DFA.unpackEncodedString(DFA11_eofS);
	static final char[] DFA11_min = DFA.unpackEncodedStringToUnsignedChars(DFA11_minS);
	static final char[] DFA11_max = DFA.unpackEncodedStringToUnsignedChars(DFA11_maxS);
	static final short[] DFA11_accept = DFA.unpackEncodedString(DFA11_acceptS);
	static final short[] DFA11_special = DFA.unpackEncodedString(DFA11_specialS);
	static final short[][] DFA11_transition;

	static {
		int numStates = DFA11_transitionS.length;
		DFA11_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA11_transition[i] = DFA.unpackEncodedString(DFA11_transitionS[i]);
		}
	}

	protected class DFA11 extends DFA {

		public DFA11(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 11;
			this.eot = DFA11_eot;
			this.eof = DFA11_eof;
			this.min = DFA11_min;
			this.max = DFA11_max;
			this.accept = DFA11_accept;
			this.special = DFA11_special;
			this.transition = DFA11_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__47 | T__48 | GEOMETRIC_VERTEX | TEXTURE_VERTEX | VERTEX_NORMAL | PARAMETER_SPACE_VERTEX | DEGREE | BASIS_MATRIX | STEP_SIZE | CURVE_SURF_TYPE | POINT | LINE | FACE | CURVE | CURVE2D | SURF | PARAM | OUTER_TRIMMING_HOLE | INNER_TRIMMING_HOLE | SPECIAL_CURVE | SPECIAL_POINT | END | CONNECT | GROUP_NAME | SMOOTHING_GROUP | MERGING_GROUP | OBJECT_NAME | BEVEL_INTERPOLATION | COLOR_INTERPOLATION | DISSOLVE_INTERPOLATION | LEVEL_OF_DETAIL | MATERIAL_NAME | MATERIAL_LIBRARY | SHADOW_CASTING | RAY_TRACING | CURVE_APPROX | SURF_APPROX | NEWLINE | DECIMAL | COMMENT | NAME | WS );";
		}
	}

}
