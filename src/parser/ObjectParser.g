/*	Wavefront's Advanced Visualizer ASCII .OBJ file format grammer.
*/

grammar ObjectParser;

@header
{
    package parser;
    import datatypes.object.*;
}

@lexer::header
{
    package parser;
    import datatypes.object.Obj;
}

start	:	(line)+ EOF
	;
	
line	:	  comment NEWLINE
            | v=vertex NEWLINE             {Obj.getObj(Obj.activeName).addV($v.v);}
            | vt=textureVertex NEWLINE     {Obj.getObj(Obj.activeName).addVT($vt.v);}
            | vn=normalVertex NEWLINE      {Obj.getObj(Obj.activeName).addVN($vn.v);}
            | f=face NEWLINE               {Obj.getObj(Obj.activeName).addFace($f.face);}
            | mlib=mtllib NEWLINE          {Material.parseMTLFile($mlib.filename);}
            | object NEWLINE
            | use_material NEWLINE
            | group NEWLINE
            | NEWLINE
	;

comment	:	COMMENT
	;
	
face returns [Face face]:   {
                                $face = new Face();
                            }
	                        FACE (x=faceVT {$face.addV($x.v); $face.addVT($x.vt);}
	                            | y=faceVTN {$face.addV($y.v); $face.addVT($y.vt); $face.addVN($y.vn);}
	                            | z=faceVN {$face.addV($z.v); $face.addVT($z.vn);})+
	;

faceVT returns [Vertex v, Vertex vt]: vIndex=DECIMAL '/' vtIndex=DECIMAL
         {
            $v = Obj.getObj(Obj.activeName).getV(Integer.parseInt($vIndex.getText()));
            $vt = Obj.getObj(Obj.activeName).getVT(Integer.parseInt($vtIndex.getText()));
         }
    ;

faceVTN returns [Vertex v, Vertex vt, Vertex vn]: vIndex=DECIMAL '/' vtIndex=DECIMAL '/' vnIndex=DECIMAL
        {
            $v = Obj.getObj(Obj.activeName).getV(Integer.parseInt($vIndex.getText()));
            $vt = Obj.getObj(Obj.activeName).getVT(Integer.parseInt($vtIndex.getText()));
            $vn = Obj.getObj(Obj.activeName).getVN(Integer.parseInt($vnIndex.getText()));
        }
    ;

faceVN returns [Vertex v, Vertex vn]: vIndex=DECIMAL '/''/' vnIndex=DECIMAL
        {
            $v = Obj.getObj(Obj.activeName).getV(Integer.parseInt($vIndex.getText()));
            $vn = Obj.getObj(Obj.activeName).getVN(Integer.parseInt(vnIndex.getText()));
        };
	
vertex	returns [Vertex v]:	GEOMETRIC_VERTEX x=DECIMAL y=DECIMAL z=DECIMAL
            {
                $v = new Vertex(Double.parseDouble(x.getText()),
                                Double.parseDouble(y.getText()),
                                Double.parseDouble(z.getText()));
            }
	;

textureVertex returns [Vertex v]: TEXTURE_VERTEX x=DECIMAL y=DECIMAL z=DECIMAL
                                    {
                                                    $v = new Vertex(Double.parseDouble(x.getText()),
                                                                    Double.parseDouble(y.getText()),
                                                                    Double.parseDouble(z.getText()));
                                    }
                                | TEXTURE_VERTEX x=DECIMAL y=DECIMAL
                                    {
                                                    $v = new Vertex(Double.parseDouble(x.getText()),
                                                                    Double.parseDouble(y.getText()),
                                                                    0);
                                    }
    ;

normalVertex returns [Vertex v]: VERTEX_NORMAL x=DECIMAL y=DECIMAL z=DECIMAL
            {
                            $v = new Vertex(Double.parseDouble(x.getText()),
                                            Double.parseDouble(y.getText()),
                                            Double.parseDouble(z.getText()));
            }
    ;
	
mtllib returns [String filename]:	MATERIAL_LIBRARY a=NAME
		{
			$filename = $a.getText();
		}
	;
	
object	:	OBJECT_NAME a=NAME
	;
	
use_material    :	MATERIAL_NAME a=NAME
    {
        Face.material_name = $a.getText();
    }
	;

group	:	SMOOTHING_GROUP (DECIMAL | 'off')
		|   GROUP_NAME a=NAME {Obj.getObj(Obj.activeName).activeGroup = $a.getText();}
		|   GROUP_NAME {Obj.getObj(Obj.activeName).activeGroup = "Default";}
	;

// Vertex data	
GEOMETRIC_VERTEX
	:	'v'
	;
	
TEXTURE_VERTEX
	:	'vt'
	;

VERTEX_NORMAL
	:	'vn'
	;

PARAMETER_SPACE_VERTEX
	:	'vp'
	;
	
// Free-form curve/surface attributes
DEGREE	:	'deg';

BASIS_MATRIX
	:	'bmat'
	;

STEP_SIZE
	:	'step'
	;
	
CURVE_SURF_TYPE
	:	'cstype'
	;
	
// Elements	
POINT	:	'p';

LINE	:	'l';

FACE	:	'f';

CURVE	:	'curv';

CURVE2D	:	'curv2';

SURF	:	'surf';

// Free-form curve/surface body statements
PARAM	:	'parm';

OUTER_TRIMMING_HOLE
	:	'trim'
	;
	
INNER_TRIMMING_HOLE
	:	'hole'
	;
	
SPECIAL_CURVE
	:	'scrv'
	;
	
SPECIAL_POINT
	:	'sp'
	;
	
END	:	'end';

// Connectivity between free-form surfaces
CONNECT	:	'con';

// Grouping
GROUP_NAME
	:	'g'
	;

SMOOTHING_GROUP
	:	's'
	;
	
MERGING_GROUP
	:	'mg'
	;
	
OBJECT_NAME
	:	'o'
	;
	
// Display/render attributes
BEVEL_INTERPOLATION
	:	'bevel'
	;

COLOR_INTERPOLATION
	:	'c_interp'
	;

DISSOLVE_INTERPOLATION
	:	'd_interp'
	;

LEVEL_OF_DETAIL
	:	'lod'
	;
	
MATERIAL_NAME
	:	'usemtl'
	;

MATERIAL_LIBRARY
	:	'mtllib'
	;

SHADOW_CASTING
	:	'shadow_obj'
	;

RAY_TRACING
	:	'trace_obj'
	;

CURVE_APPROX
	:	'ctech'
	;

SURF_APPROX
	:	'stech'
	;

fragment
DIGIT: '0'..'9';

NEWLINE : '\r'? '\n';

fragment INTEGER	:	'-'? (DIGIT)+;

fragment
EXPONENT: ('e' | 'E') ('+')? (INTEGER) ;

DECIMAL : INTEGER ('.' (DIGIT)* (EXPONENT)?)?
;

COMMENT	: '#' ~('\n'|'\r')* {skip();}
	;
	
NAME	:	( 'A'..'Z' | 'a'..'z' | '0'..'9' | '.' | '-' | '_' | '~'| '(' | ')')+
	;

WS: (' ' | '\t')+ {skip();}
;
